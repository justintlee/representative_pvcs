import math
import numpy as np
import pandas as pd
import scipy.signal as sp
import sklearn.decomposition as skd
import regex as re
import matplotlib.pyplot as plt

from scipy.stats import kurtosis
from sklearn.mixture import GaussianMixture
from sklearn.cluster import AgglomerativeClustering, KMeans
from sklearn.preprocessing import MinMaxScaler
from sklearn.impute import SimpleImputer
from statsmodels.regression.linear_model import burg
from fastdtw import fastdtw
from scipy.spatial.distance import euclidean


def extract_features(s, fs, ann, loc,
                     normalize_feats=True,
                     impute_missing=True,
                     truncate_rr=True):
    """Extracts features from time series that are relevant to V beat detection

    Input:
    s   -- signal
    fs  -- signal sampling frequency
    ann -- beat annotations
    loc -- beat locations
    fNormalizeFeats -- normalizes features to [-1, 1] (default: True)
    fImpute         -- imputes missing values in feature matrix (default: True)
    fShortenRR      -- truncates RR-intervals to [0, 2] (default: True)

    Output:
    featMat - DataFrame of features extracted for each beat
    """

    # PRE-PROCESSING
    # set parameters
    freqRes = 5
    mSamples = np.shape(s)[0]
    mDetBeats = np.shape(loc)[0]
    sampSlice = int(math.floor(0.15 * fs))
    searchLen = int(math.floor(2/3 * fs))
    backSearch = int(math.floor(7/20 * searchLen))
    forwSearch = int(searchLen - backSearch - 1)

    # filter out baseline wander
    s = sp.savgol_filter(x=s,
                         window_length=5,
                         polyorder=3)

    sbase = sp.savgol_filter(x=s,
                             window_length=fs + 1,
                             polyorder=3)

    s = s - sbase

    bHP = sp.firwin(numtaps=fs + 1,
                    cutoff=1.0/(fs/2),
                    pass_zero=False)

    s = sp.filtfilt(bHP, 1, s)

    # filter out high frequency noise
    bLP, aLP = sp.butter(N=6,
                         Wn=45/(fs/2),
                         btype="lowpass")

    s = sp.filtfilt(bLP, aLP, s)

    # generate a Hanning window with 1000 samples
    hWindow = sp.windows.gaussian(sampSlice, 10)

    # generate an array to store signal energy
    sigEn = np.zeros(mSamples)

    # perform energy calculation on entire signal
    sigEn = gteo(s,
                 m=7)

    # convolve signal with Hanning window
    sigEn = sp.convolve(sigEn, hWindow,
                        mode="same")

    # FEATURE EXTRACTION
    featMat = pd.DataFrame()

    enBeats = np.zeros(mDetBeats, dtype=float)
    sigEnInt = np.zeros(mDetBeats, dtype=float)
    dxSigMax = np.zeros(mDetBeats, dtype=float)
    dxSigMin = np.zeros(mDetBeats, dtype=float)
    sigHigh = np.zeros(mDetBeats, dtype=float)
    sigLow = np.zeros(mDetBeats, dtype=float)
    sigWidth = np.zeros(mDetBeats, dtype=float)
    sigAreaAbove = np.zeros(mDetBeats, dtype=float)
    sigAreaBelow = np.zeros(mDetBeats, dtype=float)
    LMMD = np.zeros(mDetBeats, dtype=float)
    MSA = np.zeros(mDetBeats, dtype=float)
    MSASum = np.zeros(mDetBeats, dtype=float)
    ent = np.zeros(mDetBeats, dtype=float)
    kurt = np.zeros(mDetBeats, dtype=float)
    ar1 = np.zeros(mDetBeats, dtype=float)
    ar2 = np.zeros(mDetBeats, dtype=float)
    Invert = np.zeros(mDetBeats, dtype=float)

    # create time vector
    t = np.arange(mSamples) / fs

    # RR intervals
    RRint = np.diff(t[loc])

    rrMethod = 1

    if rrMethod == 1:
        RRintp = np.insert(RRint, 0, t[loc[0]] - t[0])
        RRintn = np.insert(RRint, RRint.shape[0], t[-1] - t[loc[-1]])

        # determine RR interval ratio
        RRIntRatio = np.divide(RRintn, RRintp)

    elif rrMethod == 2:
        RRint = np.insert(RRint, 0, t[loc[0]] - t[0])

        normalMeans = np.mean(RRint[[annSym == "N" for annSym in ann]])

        # determine RR interval ratio
        RRIntRatio = np.divide(RRint, normalMeans)

    if truncate_rr:
        RRIntRatio[np.greater(RRIntRatio, 2)] = 2

    for k in range(mDetBeats):
        backInd = max(loc[k] - backSearch, 0)
        forwInd = min(loc[k] + forwSearch, mSamples - 1)
        searchWind = np.arange(backInd, forwInd)
        searchSeg = s[searchWind]

        # determine beat location energy
        enBeats[k] = np.max(sigEn[searchWind])

        # determine integrated local energy
        sigEnInt[k] = np.trapz(sigEn[searchWind])

        # get max and min of derivative
        dxS = np.diff(searchSeg)
        dxSigMax[k] = np.max(dxS)
        dxSigMin[k] = np.min(dxS)

        # obtain max and min in window for each beat
        sigHigh[k] = np.max(searchSeg)
        sigLow[k] = np.min(searchSeg)

        # determine beat width
        maxLoc = np.argmax(searchSeg)

        # obtain closest valleyn left hand side
        for leftDemark in range(maxLoc - 1, 0, -1):
            if dxS[leftDemark] > 0 and dxS[leftDemark - 1] < 0:
                break

        # obtain closest valley on right hand side
        for rightDemark in range(maxLoc + 1, searchSeg.shape[0] - 2, 1):
            if dxS[rightDemark] > 0 and dxS[rightDemark + 1] < 0:
                break

        try:
            sigWidth[k] = (rightDemark - leftDemark) / fs
        except:
            sigWidth[k] = 0

        # determine beat above/below ratio
        if (k == 0) or (k == mDetBeats - 1):
            sigAreaAbove[k] = 0
            sigAreaBelow[k] = 0
        else:
            try:
                sigAreaAbove[k] = np.trapz(np.abs(s[leftDemark:maxLoc]))
                sigAreaBelow[k] = np.trapz(np.abs(s[maxLoc:rightDemark]))
            except:
                sigAreaAbove[k] = 0
                sigAreaBelow[k] = 0

        # frequency domain feature
        fxx, Pxx = sp.periodogram(x=searchSeg,
                                  fs=fs,
                                  nfft=freqRes*fs,
                                  return_onesided=True)
        Pxx2to5 = Pxx[np.where(fxx == 2.0)[0][0]:np.where(fxx == 5.0)[0][0]]
        MSA[k] = np.max(Pxx2to5)
        MSASum[k] = np.sum(Pxx2to5)

        # entropy feature
        probPxx = np.divide(Pxx, np.sum(Pxx)) + 10e-10
        ent[k] = -np.sum(np.multiply(probPxx, np.log(probPxx)))
        if np.isnan(ent[k]) > 0:
            ent[k] = 0

        # kurtosis
        kurt[k] = kurtosis(a=searchSeg)

        # AR
        rho, _ = burg(endog=searchSeg,
                      order=2)

        ar1[k] = rho[0]
        ar2[k] = rho[1]

        # Running average, V beat can't be first
        searchSeg1 = searchSeg[0:len(searchSeg)-1]
        if k == 0:
            Smean = []
            Smean1 = []
            Nmean1 = []
            Nmean = []
            Smean.append(np.nanmean(searchSeg1[np.abs(dxS) < 0.05]))
            Smean1 = Smean
            if ann[k] == "N":
                if np.abs(sigHigh[k]-Smean) < np.abs(sigLow[k]-Smean):
                    Nmean1.append(sigLow[k])
                    Nmean = Nmean1
                elif np.abs(sigHigh[k]-Smean) > np.abs(sigLow[k]-Smean):
                    Nmean1.append(sigHigh[k])
                    Nmean = Nmean1
            else:
                Nmean1.append(np.nanmean(s[loc[ann == "N"]]))
                Nmean = np.nanmean(s[loc[ann == "N"]])
        elif k != 0:
            Smean1.append(np.nanmean(searchSeg1[np.abs(dxS) < 0.05]))
            Smean = np.nanmean(Smean1)
            if ann[k] == "N":
                if np.abs(sigHigh[k]-Smean) < np.abs(sigLow[k]-Smean):
                    Nmean1.append(sigLow[k])
                    Nmean = np.nanmean(Nmean1)
                elif np.abs(sigHigh[k]-Smean) > np.abs(sigLow[k]-Smean):
                    Nmean1.append(sigHigh[k])
                    Nmean = np.nanmean(Nmean1)

        # Signal Inversion
        if Nmean > Smean and (np.abs(sigHigh[k]-np.mean(searchSeg1[np.abs(dxS)<0.05])) < np.abs(sigLow[k]-np.mean(searchSeg1[np.abs(dxS)<.05]))):
            Invert[k] = -np.abs(sigLow[k]-np.mean(searchSeg1[np.abs(dxS)<0.05]))
        elif Nmean < Smean and (np.abs(sigHigh[k]-np.mean(searchSeg1[np.abs(dxS)<0.05])) > np.abs(sigLow[k]-np.mean(searchSeg1[np.abs(dxS)<.05]))):
            Invert[k] = np.abs(sigHigh[k]-np.mean(np.abs(dxS)<0.05))
        elif Nmean > Smean and (np.abs(sigHigh[k]-np.mean(searchSeg1[np.abs(dxS)<0.05])) > np.abs(sigLow[k]-np.mean(searchSeg1[np.abs(dxS)<.05]))):
            Invert[k] = np.abs(sigHigh[k]-np.mean(searchSeg1[np.abs(dxS)<0.05]))
        elif Nmean < Smean and (np.abs(sigHigh[k]-np.mean(searchSeg1[np.abs(dxS)<0.05])) > np.abs(sigLow[k]-np.mean(searchSeg1[np.abs(dxS)<.05]))):
            Invert[k] = -np.abs(sigLow[k]-np.mean(searchSeg1[np.abs(dxS)<0.05]))

        # time domain feature (LMMD)
        LMMD[k] = sigHigh[k] - 2*np.mean(searchSeg1[np.abs(dxS) < 0.05]) - sigLow[k]

    featMat["enBeats"] = enBeats
    featMat["sigEnInt"] = sigEnInt
    featMat["dxSigMax"] = dxSigMax
    featMat["dxSigMin"] = dxSigMin
    featMat["sigHigh"] = sigHigh
    featMat["sigLow"] = sigLow
    featMat["sigWidth"] = sigWidth
    featMat["sigAreaAbove"] = sigAreaAbove
    featMat["sigAreaBelow"] = sigAreaBelow
    featMat["LMMD"] = LMMD
    featMat["MSA"] = MSA
    featMat["MSASum"] = MSASum
    featMat["entropy"] = ent
    featMat["kurtosis"] = kurt
    featMat["ar1"] = ar1
    featMat["ar2"] = ar2
    featMat["Invert"] = Invert
    featMat["RRIntRatio"] = RRIntRatio

    # scale data to lie between -1 and 1
    if normalize_feats:
        scaler = MinMaxScaler(feature_range=(-1, 1))
        featMat.iloc[:, :] = scaler.fit_transform(featMat.iloc[:, :])

    # impute missing values
    if impute_missing:
        imp = SimpleImputer(missing_values=np.nan,
                            strategy="median")
        featMat.iloc[:, :] = imp.fit_transform(featMat.iloc[:, :])

    return featMat, s


def feat_select(feat_for_class, v_beat_array, alpha=0.3):
    """Selects features based on separability at alpha/2 and 1 - alpha/2
    quartiles. If no features are separable, then all features are output

    Input:
    feat_for_class -- DataFrame of features extracted for each beat
    v_beat_array   -- boolean array of V beats
    alpha          -- significance level for separability (default: 0.30)

    Output:
    feat_for_class -- returned DataFrame of features selected

    """

    # create feature parameters
    features_to_keep = feat_for_class.columns.to_list()
    num_features = len(features_to_keep)

    # perform adaptive feature selection
    keep_bool_array = np.zeros(num_features)

    for k in range(num_features):

        # get feature vector for non-PVCs and PVCs
        feat_mat_false_beat = feat_for_class[np.invert(v_beat_array)]
        feat_false_beat = feat_mat_false_beat[features_to_keep[k]]

        feat_mat_true_beat = feat_for_class[v_beat_array]
        feat_true_beat = feat_mat_true_beat[features_to_keep[k]]

        # determine quartiles for each group
        feat_group0_lo = feat_false_beat.quantile(alpha / 2)
        feat_group0_hi = feat_false_beat.quantile(1 - alpha / 2)

        feat_group1_lo = feat_true_beat.quantile(alpha / 2)
        feat_group1_hi = feat_true_beat.quantile(1 - alpha / 2)

        # determine whether feature should be kept
        group1_higher = feat_group0_hi < feat_group1_lo
        group0_higher = feat_group0_lo > feat_group1_hi

        if (group1_higher) or (group0_higher):
            keep_bool_array[k] = int(1)

    num_features_to_keep = np.sum(keep_bool_array)

    # remove features that are not well separable
    if num_features_to_keep > 0:
        print("%d features selected as separable with alpha = %.2f"
              % (num_features_to_keep, alpha))

        keep_bool_array = np.where(keep_bool_array != 0)[0].tolist()
        feat_for_class = feat_for_class.iloc[:, keep_bool_array]

    # call everything to be the negative class if not well separable
    else:
        print("no features are separable with alpha = %.2f"
              % (alpha))

    return feat_for_class


def classify_v(featMat, ann, loc, fs, s,
               fPCA=True,
               f2ndFilter=True):
    """Determines which beats are V beats given a feature matrix

    Input:
    featMat -- DataFrame of features extracted for each beat
    ann     -- original beat annotations
    f2ndFilter -- uses classify_v as a second stage filter (default: True)

    Output:
    detectedPvcs -- boolean array of which beats are PVCs

    """

    # parameters
    alpha = 2
    num_samples = s.shape[0]
    n_components = 1
    n0_threshold = 0.30

    # create boolean array of previously detected PVCs
    tpvc = (ann == "V")
    num_beats = np.shape(tpvc)[0]

    # sparse dimensionality reduction
    if fPCA and featMat.shape[1] >= n_components:
        pca = skd.PCA(n_components=n_components,
                      tol=1e-6,
                      random_state=0)
        featMat = pca.fit_transform(featMat.copy())

    # create prototype Gaussian mixture model
    gmm = GaussianMixture(n_components=4,
                          covariance_type="full",
                          tol=1e-6,
                          max_iter=int(1e6),
                          init_params="random",
                          random_state=0)

    # first stage clustering using GMM
    gmmRes = gmm.fit(featMat)
    gmmClustRes = gmmRes.predict(featMat)

    # second stage clustering using hierarchical distance on GMM means
    hClustGmmPCA = AgglomerativeClustering(n_clusters=2,
                                           linkage="single")
    hClustGmmPCARes = hClustGmmPCA.fit_predict(gmmRes.means_)

    # associate hierarchical cluster results to GMM results
    class_array = np.zeros(featMat.shape[0])

    for k in set(gmmClustRes):
        class_array[np.where(gmmClustRes == k)] = hClustGmmPCARes[k]

    class_array = class_array.astype(bool)

    # decide which cluster is PVC based on amount of N annotations cluster
    # contains
    group0Sum = np.sum(np.logical_and(tpvc, class_array))
    group1Sum = np.sum(np.logical_and(tpvc, np.invert(class_array)))
    if group0Sum < group1Sum:
        class_array = np.invert(class_array)

    # perform check on V cluster to determine whether there are a substantial
    # number of normal beats identified by first stage in it
    # count # of N0's in V cluster
    num_n0_in_v_cluster = np.sum(np.logical_and(np.invert(tpvc), class_array))
    n0_ratio_in_v_cluster = num_n0_in_v_cluster/num_beats

    # check N0 ratio in V cluster
    if n0_ratio_in_v_cluster > n0_threshold:
        v_cluster_loc = np.where(class_array)[0]
        n_cluster_loc = np.where(np.invert(class_array))[0]

        # if N0's high, make all beats in the V cluster N beats
        class_array[v_cluster_loc] = False

        # make the beats in the N cluster equal to the value of their original
        # labels
        class_array[n_cluster_loc] = tpvc[n_cluster_loc]

        back_wind = int(math.floor(0.15 * fs))
        forw_wind = int(math.floor(0.15 * fs))
        samples_in_wind = back_wind + forw_wind

        normal_stack = np.zeros((np.sum(np.invert(tpvc)), samples_in_wind))

        for k, n_loc in enumerate(loc[np.invert(tpvc)]):

            back_ind = max(n_loc - back_wind, 0)
            forw_ind = min(n_loc + forw_wind, num_samples - 1)

            s_current = s[back_ind:forw_ind]

            if s_current.shape[0] != samples_in_wind:
                if n_loc < int(math.floor((num_samples - 1)/2)):
                    samples_to_add = samples_in_wind - s_current.shape[0]
                    s_current = np.append(np.zeros(samples_to_add), s_current)

                elif n_loc > int(math.floor((num_samples - 1)/2)):
                    samples_to_add = samples_in_wind - s_current.shape[0]
                    s_current = np.append(s_current, np.zeros(samples_to_add))

            normal_stack[k, :] = s_current

        normal_mean_array = np.mean(normal_stack, axis=0)

        normal_distance_array = np.zeros(normal_stack.shape[0])

        for k in range(normal_stack.shape[0]):
            distance, _ = fastdtw(normal_mean_array,
                                  normal_stack[k, :],
                                  dist=euclidean)
            normal_distance_array[k] = distance

        normal_mean = normal_distance_array.mean()
        normal_std = normal_distance_array.std()

        vent_stack = np.zeros((np.sum(tpvc), samples_in_wind))

        for k, v_loc in enumerate(loc[tpvc]):

            back_ind = max(v_loc - back_wind, 0)
            forw_ind = min(v_loc + forw_wind, num_samples - 1)

            s_current = s[back_ind:forw_ind]

            if s_current.shape[0] != samples_in_wind:
                if v_loc < int(math.floor((num_samples - 1)/2)):
                    samples_to_add = samples_in_wind - s_current.shape[0]
                    s_current = np.append(np.zeros(samples_to_add), s_current)

                elif v_loc > int(math.floor((num_samples - 1)/2)):
                    samples_to_add = samples_in_wind - s_current.shape[0]
                    s_current = np.append(s_current, np.zeros(samples_to_add))

            vent_stack[k, :] = s_current

        vent_distance_array = np.empty([0])
        vent_filter = np.ones(vent_stack.shape[0]).astype(bool)

        for k in range(vent_stack.shape[0]):
            distance, _ = fastdtw(normal_mean_array,
                                  vent_stack[k, :],
                                  dist=euclidean)
            vent_distance_array = np.append(vent_distance_array, distance)

            if abs(distance - normal_mean) < alpha * normal_std:
                vent_filter[k] = False

        class_array = tpvc
        class_array[tpvc] = vent_filter

    # turn all N's back into N's
    if f2ndFilter:
        normalLoc = np.where(np.invert(tpvc))[0]
        class_array[normalLoc] = False

    return class_array


def check_for_v_runs(class_array, loc, s, fs, orig_ann, f2ndFilter):
    """Checks patterns consisting of NVVVN, VNVV, VVNV, VNNVV, VVNNV, VNNV, to
    make sure that they are not actually VVVV or VVVVV

    Input:
    class_array -- classification array generated by classify_v
    loc         -- beat locations
    s           -- ECG signal
    fs          -- sampling frequency
    orig_ann    -- original annotations

    Output:
    class_array -- boolean array of which beats are PVCs

    """

    # turn all N's back into N's
    if f2ndFilter:
        normal_loc = np.where(np.invert(orig_ann))[0]
        class_array[normal_loc] = False

    # parameters
    alpha = 2
    max_ind = s.shape[0] - 1
    search_len = int(math.floor(2/3 * fs))
    back_search = int(math.floor(7/20 * search_len))
    forw_search = int(search_len - back_search - 1)

    sig_seg_len = back_search + forw_search

    back_loc = np.array([max(ind - back_search, 0) for ind in loc])
    forw_loc = np.array([min(ind + forw_search, max_ind) for ind in loc])

    # get windowed signal around every beat
    beat_stack = np.zeros((class_array.shape[0], sig_seg_len))

    for k in range(loc.shape[0]):

        s_current = s[back_loc[k]:forw_loc[k]]

        if s_current.shape[0] != sig_seg_len:
            if k < 20:
                samples_to_add = sig_seg_len - s_current.shape[0]
                s_current = np.append(np.zeros(samples_to_add), s_current)

            elif k > 20:
                samples_to_add = sig_seg_len - s_current.shape[0]
                s_current = np.append(s_current, np.zeros(samples_to_add))

        beat_stack[k, :] = s_current

    # determine what the mean normal beat looks like
    normal_beat_mean_array = np.mean(beat_stack[np.invert(orig_ann), :],
                                     axis=0)

    beat_dist_array = np.zeros(orig_ann.shape[0])

    for k in range(orig_ann.shape[0]):

        beat_dist_array[k] = euclidean(beat_stack[k, :],
                                       normal_beat_mean_array)

    normal_dist_mean = beat_dist_array[np.invert(orig_ann)].mean()
    normal_dist_std = beat_dist_array[np.invert(orig_ann)].std()

#    plt.figure()
#    plt.hist(beat_dist_array, 50, (0, 20))
#    plt.show()

#    print(normal_dist_mean)
#    print(normal_dist_std)

    # convert boolean array to string list in order to utilize regex
    pos_string = ''.join(class_array.astype(int).astype(str).tolist())

    # detect patterns
    pat_01110 = np.array([match.span()[0] - 1
                          for match in re.finditer(r'(?<=(01110))',
                         pos_string,
                         overlapped=True)])
    pat_01010 = np.array([match.span()[0] - 1
                          for match in re.finditer(r'(?<=(01010))',
                         pos_string,
                         overlapped=True)])
    pat_01001 = np.array([match.span()[0] - 1
                          for match in re.finditer(r'(?<=(01001))',
                         pos_string,
                         overlapped=True)])
    pat_00000 = np.array([match.span()[0] - 1
                          for match in re.finditer(r'(?<=(00000))',
                         pos_string,
                         overlapped=True)])
    pat_01011 = np.array([match.span()[0] - 1
                          for match in re.finditer(r'(?<=(01011))',
                         pos_string,
                         overlapped=True)])
    pat_01101 = np.array([match.span()[0] - 1
                         for match in re.finditer(r'(?<=(01101))',
                         pos_string,
                         overlapped=True)])
    pat_10011 = np.array([match.span()[0] - 1
                          for match in re.finditer(r'(?<=(10011))',
                         pos_string,
                         overlapped=True)])
    pat_11001 = np.array([match.span()[0] - 1
                          for match in re.finditer(r'(?<=(11001))',
                         pos_string,
                         overlapped=True)])
    pat_11011 = np.array([match.span()[0] - 1
                          for match in re.finditer(r'(?<=(11011))',
                         pos_string,
                         overlapped=True)])

    # fix NVVVN pattern if necessary:
    for k in pat_01110:
        s1 = s[back_loc[k - 4]:forw_loc[k - 4]]
        s1_len_ch = s1.shape[0] != sig_seg_len

        s2 = s[back_loc[k - 3]:forw_loc[k - 3]]
        s2_len_ch = s2.shape[0] != sig_seg_len

        s3 = s[back_loc[k - 2]:forw_loc[k - 2]]
        s3_len_ch = s3.shape[0] != sig_seg_len

        s4 = s[back_loc[k - 1]:forw_loc[k - 1]]
        s4_len_ch = s4.shape[0] != sig_seg_len

        s5 = s[back_loc[k]:forw_loc[k]]
        s5_len_ch = s5.shape[0] != sig_seg_len

        # check to make sure signals are all of the same length in order
        # to perform proper cross correlation
        if s1_len_ch or s2_len_ch or s3_len_ch or s4_len_ch or s5_len_ch:
            continue

        corr21 = np.greater(np.correlate(s2, s1), 0).astype(int)
        corr31 = np.greater(np.correlate(s3, s1), 0).astype(int)
        corr41 = np.greater(np.correlate(s4, s1), 0).astype(int)

        corr25 = np.greater(np.correlate(s2, s5), 0).astype(int)
        corr35 = np.greater(np.correlate(s3, s5), 0).astype(int)
        corr45 = np.greater(np.correlate(s4, s5), 0).astype(int)

        # same direction as identified PVCs
        if corr21 + corr31 + corr41 > 1:
#            print("here")
            class_array[k - 4] = True

        if corr25 + corr35 + corr45 > 1:
            class_array[k] = True

#        plt.figure()
#        plt.plot(normal_beat_mean_array, label="normal mean")
#        plt.plot(s1, label="beat1")
#        plt.plot(s5, label="beat5")
#        plt.legend()
#        plt.show()

#        print(loc[k] / fs)
#        print(np.correlate(s2, s1))
#        print(np.correlate(s3, s1))
#        print(np.correlate(s4, s1))
#        print(np.correlate(s2, s5))
#        print(np.correlate(s3, s5))
#        print(np.correlate(s4, s5))
#        print(euclidean(s1, s5))

        # different direction from identified PVCs
        if euclidean(s1, s5) - normal_dist_mean > alpha * normal_dist_std:
            if (euclidean(s1, normal_beat_mean_array) - normal_dist_mean) > (alpha * normal_dist_std):
                class_array[k - 4] = True
            if (euclidean(s5, normal_beat_mean_array) - normal_dist_mean) > (alpha * normal_dist_std):
                class_array[k] = True

    # fix NVNVN pattern if necessary:
    for k in pat_01010:
        s1 = s[back_loc[k - 4]:forw_loc[k - 4]]
        s1_len_ch = s1.shape[0] != sig_seg_len

        s2 = s[back_loc[k - 3]:forw_loc[k - 3]]
        s2_len_ch = s2.shape[0] != sig_seg_len

        s3 = s[back_loc[k - 2]:forw_loc[k - 2]]
        s3_len_ch = s3.shape[0] != sig_seg_len

        s4 = s[back_loc[k - 1]:forw_loc[k - 1]]
        s4_len_ch = s4.shape[0] != sig_seg_len

        s5 = s[back_loc[k]:forw_loc[k]]
        s5_len_ch = s5.shape[0] != sig_seg_len

        # check to make sure signals are all of the same length in order
        # to perform proper cross correlation
        if s1_len_ch or s2_len_ch or s3_len_ch or s4_len_ch or s5_len_ch:
            continue

        corr23 = np.greater(np.correlate(s2, s3), 0).astype(int)
        corr43 = np.greater(np.correlate(s4, s3), 0).astype(int)

        corr25 = np.greater(np.correlate(s2, s5), 0).astype(int)
        corr45 = np.greater(np.correlate(s4, s5), 0).astype(int)

        if corr23 + corr43 > 1:
            class_array[k - 2] = True

            corr35 = np.greater(np.correlate(s3, s5), 0).astype(int)

            if corr25 + corr35 + corr45 == 3:
                class_array[k] = True

            corr21 = np.greater(np.correlate(s3, s5), 0).astype(int)
            corr31 = np.greater(np.correlate(s3, s5), 0).astype(int)
            corr41 = np.greater(np.correlate(s3, s5), 0).astype(int)

            if corr21 + corr31 + corr41 == 3:
                class_array[k - 4] = True

    # fix NVNNV pattern if necessary:
    for k in pat_01001:
        s1 = s[back_loc[k - 4]:forw_loc[k - 4]]
        s1_len_ch = s1.shape[0] != sig_seg_len

        s2 = s[back_loc[k - 3]:forw_loc[k - 3]]
        s2_len_ch = s2.shape[0] != sig_seg_len

        s3 = s[back_loc[k - 2]:forw_loc[k - 2]]
        s3_len_ch = s3.shape[0] != sig_seg_len

        s4 = s[back_loc[k - 1]:forw_loc[k - 1]]
        s4_len_ch = s4.shape[0] != sig_seg_len

        s5 = s[back_loc[k]:forw_loc[k]]
        s5_len_ch = s5.shape[0] != sig_seg_len

        # check to make sure signals are all of the same length in order
        # to perform proper cross correlation
        if s1_len_ch or s2_len_ch or s3_len_ch or s4_len_ch or s5_len_ch:
            continue

        corr23 = np.greater(np.correlate(s2, s3), 0).astype(int)
        corr53 = np.greater(np.correlate(s5, s3), 0).astype(int)

        corr24 = np.greater(np.correlate(s2, s4), 0).astype(int)
        corr54 = np.greater(np.correlate(s5, s4), 0).astype(int)

        if corr23 + corr53 > 1:
            class_array[k - 2] = True

            corr34 = np.greater(np.correlate(s3, s4), 0).astype(int)

            if corr23 + corr34 + corr53 == 3:
                class_array[k - 1] = True

            corr21 = np.greater(np.correlate(s3, s5), 0).astype(int)
            corr31 = np.greater(np.correlate(s3, s5), 0).astype(int)
            corr51 = np.greater(np.correlate(s3, s5), 0).astype(int)

            if corr21 + corr31 + corr51 == 3:
                class_array[k - 4] = True

#    # fix NNNNN pattern if necessary:
#    for k in pat_00000:
#        s1 = s[back_loc[k - 4]:forw_loc[k - 4]]
#        s1_len_ch = s1.shape[0] != sig_seg_len
#
#        s2 = s[back_loc[k - 3]:forw_loc[k - 3]]
#        s2_len_ch = s2.shape[0] != sig_seg_len
#
#        s3 = s[back_loc[k - 2]:forw_loc[k - 2]]
#        s3_len_ch = s3.shape[0] != sig_seg_len
#
#        s4 = s[back_loc[k - 1]:forw_loc[k - 1]]
#        s4_len_ch = s4.shape[0] != sig_seg_len
#
#        s5 = s[back_loc[k]:forw_loc[k]]
#        s5_len_ch = s5.shape[0] != sig_seg_len
#
#        # check to make sure signals are all of the same length in order
#        # to perform proper cross correlation
#        if s1_len_ch or s2_len_ch or s3_len_ch or s4_len_ch or s5_len_ch:
#            continue
#
#        if not orig_ann[k - 4] \
#           or not orig_ann[k - 3] \
#           or not orig_ann[k - 2] \
#           or not orig_ann[k - 1]\
#           or not orig_ann[k]:
#            continue
#
#        corr12 = np.greater(np.correlate(s1, s2), 0).astype(int)
#
#        if not corr12:
#
#            corr23 = np.greater(np.correlate(s2, s3), 0).astype(int)
#            corr24 = np.greater(np.correlate(s2, s4), 0).astype(int)
#            corr25 = np.greater(np.correlate(s2, s5), 0).astype(int)
#            corr34 = np.greater(np.correlate(s3, s4), 0).astype(int)
#            corr35 = np.greater(np.correlate(s3, s5), 0).astype(int)
#            corr45 = np.greater(np.correlate(s4, s5), 0).astype(int)
#
#            if corr23 + corr24 + corr25 + corr34 + corr35 + corr45 == 6:
#                class_array[k - 3] = True
#                class_array[k - 2] = True
#                class_array[k - 1] = True
#                class_array[k] = True

    # fix NVNVV pattern if necessary:
    for k in pat_01011:
        s1 = s[back_loc[k - 4]:forw_loc[k - 4]]
        s1_len_ch = s1.shape[0] != sig_seg_len

        s2 = s[back_loc[k - 3]:forw_loc[k - 3]]
        s2_len_ch = s2.shape[0] != sig_seg_len

        s3 = s[back_loc[k - 2]:forw_loc[k - 2]]
        s3_len_ch = s3.shape[0] != sig_seg_len

        s4 = s[back_loc[k - 1]:forw_loc[k - 1]]
        s4_len_ch = s4.shape[0] != sig_seg_len

        s5 = s[back_loc[k]:forw_loc[k]]
        s5_len_ch = s5.shape[0] != sig_seg_len

        # check to make sure signals are all of the same length in order
        # to perform proper cross correlation
        if s1_len_ch or s2_len_ch or s3_len_ch or s4_len_ch or s5_len_ch:
            continue

        corr23 = np.greater(np.correlate(s2, s3), 0).astype(int)
        corr43 = np.greater(np.correlate(s4, s3), 0).astype(int)
        corr53 = np.greater(np.correlate(s5, s3), 0).astype(int)

        if corr23 + corr43 + corr53 > 1:
            class_array[k - 2] = True

    # fix NVVNV pattern if necessary:
    for k in pat_01101:
        s1 = s[back_loc[k - 4]:forw_loc[k - 4]]
        s1_len_ch = s1.shape[0] != sig_seg_len

        s2 = s[back_loc[k - 3]:forw_loc[k - 3]]
        s2_len_ch = s2.shape[0] != sig_seg_len

        s3 = s[back_loc[k - 2]:forw_loc[k - 2]]
        s3_len_ch = s3.shape[0] != sig_seg_len

        s4 = s[back_loc[k - 1]:forw_loc[k - 1]]
        s4_len_ch = s4.shape[0] != sig_seg_len

        s5 = s[back_loc[k]:forw_loc[k]]
        s5_len_ch = s5.shape[0] != sig_seg_len

        # check to make sure signals are all of the same length in order
        # to perform proper cross correlation
        if s1_len_ch or s2_len_ch or s3_len_ch or s4_len_ch or s5_len_ch:
            continue

        corr21 = np.greater(np.correlate(s2, s1), 0).astype(int)
        corr31 = np.greater(np.correlate(s3, s1), 0).astype(int)
        corr51 = np.greater(np.correlate(s5, s1), 0).astype(int)

        corr24 = np.greater(np.correlate(s2, s4), 0).astype(int)
        corr34 = np.greater(np.correlate(s3, s4), 0).astype(int)
        corr54 = np.greater(np.correlate(s5, s4), 0).astype(int)

#        print(loc[k] / fs)
#        print(corr21)
#        print(corr31)
#        print(corr51)
#
#        print(corr24)
#        print(corr34)
#        print(corr54)

        if corr21 + corr31 + corr51 > 1:
            class_array[k - 4] = True

        if corr24 + corr34 + corr54 > 1:
            class_array[k - 1] = True

    # fix VNNVV pattern if necessary:
    for k in pat_10011:
        s1 = s[back_loc[k - 4]:forw_loc[k - 4]]
        s1_len_ch = s1.shape[0] != sig_seg_len

        s2 = s[back_loc[k - 3]:forw_loc[k - 3]]
        s2_len_ch = s2.shape[0] != sig_seg_len

        s3 = s[back_loc[k - 2]:forw_loc[k - 2]]
        s3_len_ch = s3.shape[0] != sig_seg_len

        s4 = s[back_loc[k - 1]:forw_loc[k - 1]]
        s4_len_ch = s4.shape[0] != sig_seg_len

        s5 = s[back_loc[k]:forw_loc[k]]
        s5_len_ch = s5.shape[0] != sig_seg_len

        # check to make sure signals are all of the same length in order
        # to perform proper cross correlation
        if s1_len_ch or s2_len_ch or s3_len_ch or s4_len_ch or s5_len_ch:
            continue

        corr12 = np.greater(np.correlate(s1, s2), 0).astype(int)
        corr42 = np.greater(np.correlate(s4, s2), 0).astype(int)
        corr52 = np.greater(np.correlate(s5, s2), 0).astype(int)

        corr13 = np.greater(np.correlate(s1, s3), 0).astype(int)
        corr43 = np.greater(np.correlate(s4, s3), 0).astype(int)
        corr53 = np.greater(np.correlate(s5, s3), 0).astype(int)

        # same direction as identified PVCs
        if corr12 + corr42 + corr52 > 1:
            class_array[k - 3] = True

        if corr13 + corr43 + corr53 > 1:
            class_array[k - 2] = True

        # different direction from identified PVCs
        if (euclidean(s2, s3) - normal_dist_mean) > (alpha * normal_dist_std):
            if (euclidean(s2, normal_beat_mean_array) - normal_dist_mean) > (alpha * normal_dist_std):
                class_array[k - 3] = True
            elif (euclidean(s3, normal_beat_mean_array) - normal_dist_mean) > (alpha * normal_dist_std):
                class_array[k - 2] = True

    # fix VNNVV pattern if necessary:
    for k in pat_11001:
        s1 = s[back_loc[k - 4]:forw_loc[k - 4]]
        s1_len_ch = s1.shape[0] != sig_seg_len

        s2 = s[back_loc[k - 3]:forw_loc[k - 3]]
        s2_len_ch = s2.shape[0] != sig_seg_len

        s3 = s[back_loc[k - 2]:forw_loc[k - 2]]
        s3_len_ch = s3.shape[0] != sig_seg_len

        s4 = s[back_loc[k - 1]:forw_loc[k - 1]]
        s4_len_ch = s4.shape[0] != sig_seg_len

        s5 = s[back_loc[k]:forw_loc[k]]
        s5_len_ch = s5.shape[0] != sig_seg_len

        # check to make sure signals are all of the same length in order
        # to perform proper cross correlation
        if s1_len_ch or s2_len_ch or s3_len_ch or s4_len_ch or s5_len_ch:
            continue

        corr13 = np.greater(np.correlate(s1, s3), 0).astype(int)
        corr23 = np.greater(np.correlate(s2, s3), 0).astype(int)
        corr53 = np.greater(np.correlate(s5, s3), 0).astype(int)

        corr14 = np.greater(np.correlate(s1, s4), 0).astype(int)
        corr24 = np.greater(np.correlate(s2, s4), 0).astype(int)
        corr54 = np.greater(np.correlate(s5, s4), 0).astype(int)

#        plt.figure()
#        plt.plot(normal_beat_mean_array, label="normal mean")
#        plt.plot(s3, label="beat3")
#        plt.plot(s4, label="beat4")
#        plt.legend()
#        plt.show()
#
#        print(loc[k] / fs)
#        print(euclidean(s3, s4))
#        print(euclidean(s3, normal_beat_mean_array))
#        print(euclidean(s4, normal_beat_mean_array))

        # same direction as identified PVCs
        if corr13 + corr23 + corr53 > 1:
            class_array[k - 2] = True

        if corr14 + corr24 + corr54 > 1:
            class_array[k - 1] = True

        # different direction from identified PVCs
        if (euclidean(s3, s4) - normal_dist_mean) > (alpha * normal_dist_std):
            if (euclidean(s3, normal_beat_mean_array) - normal_dist_mean) > (alpha * normal_dist_std):
                class_array[k - 2] = True
            elif (euclidean(s4, normal_beat_mean_array) - normal_dist_mean) > (alpha * normal_dist_std):
                class_array[k - 1] = True

    # fix VVNVV pattern if necessary:
    for k in pat_11011:
        s1 = s[back_loc[k - 4]:forw_loc[k - 4]]
        s1_len_ch = s1.shape[0] != sig_seg_len

        s2 = s[back_loc[k - 3]:forw_loc[k - 3]]
        s2_len_ch = s2.shape[0] != sig_seg_len

        s3 = s[back_loc[k - 2]:forw_loc[k - 2]]
        s3_len_ch = s3.shape[0] != sig_seg_len

        s4 = s[back_loc[k - 1]:forw_loc[k - 1]]
        s4_len_ch = s4.shape[0] != sig_seg_len

        s5 = s[back_loc[k]:forw_loc[k]]
        s5_len_ch = s5.shape[0] != sig_seg_len

        # check to make sure signals are all of the same length in order
        # to perform proper cross correlation
        if s1_len_ch or s2_len_ch or s3_len_ch or s4_len_ch or s5_len_ch:
            continue

        corr13 = np.greater(np.correlate(s1, s3), 0).astype(int)
        corr23 = np.greater(np.correlate(s2, s3), 0).astype(int)
        corr43 = np.greater(np.correlate(s4, s3), 0).astype(int)
        corr53 = np.greater(np.correlate(s5, s3), 0).astype(int)

        # same direction as identified PVCs
        if corr13 + corr23 + corr43 + corr53 > 2:
            class_array[k - 2] = True

    return class_array


def gteo(x, m=1):
    """Extracts the local energy of a time series signal through GTEO.float

    Input:
    x -- signal
    m -- order (default: 1)

    Output:
    xe - energy signal
    """

    mSamp = x.shape[0]

    xe = np.zeros(mSamp)

    for k in range(mSamp):
        if (k - m) < 0:
            xe[k] = 0
        elif (k + m) > (mSamp - 1):
            xe[k] = 0
        else:
            xe[k] = (x[k] ** 2) - (x[k - m] * x[k + m])

    return xe
