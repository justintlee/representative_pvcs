# IMPORT LIBRARIES ############################################################

import os
import wfdb
import pandas as pd
import regex as re
import numpy as np
import parse_methods

###############################################################################

# FUNCTIONS ###################################################################


def interpolate_time_stamps(ts, fs):

    num_samples = ts.shape[0]
    num_seconds = int(num_samples / fs)
    num_rem = num_samples % fs

    t = ts.copy()

    base_time_stamp_array = np.arange(fs) / fs

    for k in range(num_seconds):
        start_ind = fs * k
        end_ind = fs * (k + 1)

        t[start_ind:end_ind] = ts[start_ind:end_ind] + base_time_stamp_array

    if num_rem:
        t[end_ind:] = ts[end_ind:] + base_time_stamp_array[:num_rem]

    return t


def ann_type_convert(ann_type):
    """Converts a numeric annotation type to a char annotation type."""
    ann_dict = {1: "N",
                5: "V",
                9: "A",
                13: "Q",
                12: "/",
                16: "|",
                39: "(",
                24: "p",
                40: ")",
                27: "t",
                29: "u",
                23: "=",
                14: "~",
                32: "[",
                33: "]",
                28: "+",
                22: '"',
                42: "C"}

    ann_type = np.array([ann_dict[k] for k in ann_type])

    return ann_type


def pattern_match(beat_string, pattern, beat_loc,
                  sig_qual_info, offset, look_back):

    # match pattern
    match_loc = np.array([match.span()[0] - 1
                          for match in re.finditer(pattern,
                                                   beat_string,
                                                   overlapped=False)])

    # ensure prematurity and compensatory pause
    ind_to_delete = np.array([])
    for array_ind, beat_ind in enumerate(match_loc):

        # ensure that the first channel is clean for all beats
        pattern_qual = True

        for ind in range(look_back + 1):
            beat_sig_qual = sig_qual_info[beat_loc[beat_ind - ind]] in (0, 2)
            pattern_qual = pattern_qual and beat_sig_qual

        if not pattern_qual:
            ind_to_delete = np.append(ind_to_delete, array_ind)

        beat_ind = beat_ind - offset

        # delete if there are no prior beats to evaluate for prematurity
        if beat_ind < 2:
            ind_to_delete = np.append(ind_to_delete, array_ind)

        # calculate premature ratio
        rr_current = beat_loc[beat_ind] - beat_loc[beat_ind - 1]
        rr_prev = beat_loc[beat_ind - 1] - beat_loc[beat_ind - 2]

        rr_ratio_premature = rr_current / rr_prev

        # if not premature then delete:
        if rr_ratio_premature > 0.8:
            ind_to_delete = np.append(ind_to_delete, array_ind)

        # ensure compensatory pause
        rr_next = beat_loc[beat_ind + 1] - beat_loc[beat_ind]

        rr_ratio_pause = rr_next / rr_prev

        # if no compensatory pause then delete:
        if rr_ratio_pause < 1.2:
            ind_to_delete = np.append(ind_to_delete, array_ind)

    # delete non-premature, non-compensatory beats, beats that are found in
    # unclean areas, or beats that have no prior reference
    match_loc = np.delete(match_loc, ind_to_delete)

    return match_loc - offset


###############################################################################

# SCRIPT PARAMETERS ###########################################################

# variables
BASE_DIR = "C:\\Users\\justin.lee\\Documents\\datasets\\2019-04 Field Data\\"
DB_ARRAY = [subfolder for subfolder in os.listdir(BASE_DIR)]
fs = 250
NUM_DAYS = 7

# flags
FLAG_OUTPUT_DATA = True
FLAG_VERBOSE = True

###############################################################################

# STATISTICS STRUCTURE ########################################################

# database pattern count summary
pattern_stats_array = pd.DataFrame(columns={"database",
                                            "v_count_cbd",
                                            "n3vn3_count_cbd"})

# database pattern count summary from each hour
instance_stats_array = pd.DataFrame(columns={"database",
                                             "group",
                                             "v_count_cbd",
                                             "n3vn3_count_cbd"})

for DB in DB_ARRAY:

    print("DB: %s" % DB)
    FILE = BASE_DIR + DB + "\\ECG_DB.db"
    ANNOTATION_FILE = DB

    # individual database pattern count summary
    pattern_stats = pd.DataFrame(columns={"database",
                                          "v_count_cbd",
                                          "n3vn3_count_cbd"})

    pattern_stats["database"] = [DB]
    pattern_stats["v_count_cbd"] = [0]
    pattern_stats["n3vn3_count_cbd"] = [0]

    if FLAG_VERBOSE:
        print("initialized Pandas data structure")

###############################################################################

# LOAD DATA ###################################################################

    # list the regex strings for different patterns
    v = r"?<=(V)"
    n3vn3 = r"(?<=(NNNVNNN))"

    # define constants used in every iteration
    nrec = 3600
    srec = 1000
#    srec = 65800
    days_in_seconds = 60 * 60 * 24 * NUM_DAYS + srec
#    days_in_seconds = 69401
    last_sig_qual = 0
    num_loops_per_dataset = 0

    # determine the total number of seconds in the database
    num_total_seconds = parse_methods.getNumPackets(FILE)

    if FLAG_VERBOSE:
        print("started loop")

    while (min(days_in_seconds, num_total_seconds) - srec) > nrec:

        # initialize text file to store TP timestamps/sample numbers
        UNIT_TEST_FILE = ".\\unit_test_data\\" \
                            + DB \
                            + "_" \
                            + str(num_loops_per_dataset) \
                            + ".txt"

        if FLAG_OUTPUT_DATA:
            f = open(UNIT_TEST_FILE, "a")
            f.close()

        # initialize count values for the current hour of data
        instance_stats = pd.DataFrame(columns={"database",
                                               "group",
                                               "v_count_cbd",
                                               "n3vn3_count_cbd"})

        instance_stats["database"] = [DB]
        instance_stats["group"] = [num_loops_per_dataset]
        instance_stats["v_count_cbd"] = [0]
        instance_stats["n3vn3_count_cbd"] = [0]

        if FLAG_VERBOSE:
            print("%s - %d" % (DB, srec))

        try:

            # get ECG data along with UTC timestamps
            s1, s2, ts = parse_methods.getECGData(FILE, nrec, srec)

            # correct UTC timestamps
            t = interpolate_time_stamps(ts, fs)

            num_samples = s1.shape[0]

            ann_loc, \
                ann_type, \
                ann_chan, \
                ann_num = parse_methods.getANNData(FILE,
                                                   nrec,
                                                   srec)
            ann_loc_gt_num_samples = ann_loc >= s1.shape[0]
            if np.sum(ann_loc_gt_num_samples) > 0:
                first_loc_greater = np.where(ann_loc_gt_num_samples)[0][0]
                ann_loc = ann_loc[:first_loc_greater]
                ann_type = ann_type[:first_loc_greater]
                ann_chan = ann_chan[:first_loc_greater]
                ann_num = ann_num[:first_loc_greater]

        except Exception as exc:

            print(exc)
            srec += nrec
            continue

        ann_type = ann_type_convert(ann_type)

        sorted_loc = np.argsort(ann_loc)
        ann_loc = ann_loc[sorted_loc]
        ann_type = ann_type[sorted_loc]
        ann_chan = ann_chan[sorted_loc]
        ann_num = ann_num[sorted_loc]
        ann_num[ann_num < 0] = 0
        ann_num[ann_num > 127] = 0

        # write to .atr file
        if FLAG_OUTPUT_DATA:
            wfdb.wrann(record_name=ANNOTATION_FILE
                       + "_"
                       + str(num_loops_per_dataset),
                       extension="atr",
                       sample=(srec * fs + ann_loc).astype(int),
                       symbol=ann_type,
                       chan=ann_chan,
                       num=ann_num,
                       fs=fs,
                       write_dir=".\\unit_test_data\\")

        # extract standard beats from the annotations
        standard_beat_types = ("V", "A", "N", "Q", "/", "|")
        beat_loc = ann_loc[[ann_inst in standard_beat_types
                            for ann_inst in ann_type]]
        beat_type = ann_type[[ann_inst in standard_beat_types
                              for ann_inst in ann_type]]
        beat_chan = ann_chan[[ann_inst in standard_beat_types
                              for ann_inst in ann_type]]
        beat_num = ann_num[[ann_inst in standard_beat_types
                            for ann_inst in ann_type]]

        # obtain a time series of signal quality values
        sig_qual_pos = np.where(np.logical_and(ann_type == "~",
                                               ann_chan == 108))[0]
        sig_qual_loc = ann_loc[sig_qual_pos]

        sig_qual_array = np.zeros(num_samples)

        for pos, loc in enumerate(sig_qual_loc):

            if pos == 0:
                sig_qual_array[:loc] = last_sig_qual

            elif pos == (sig_qual_loc.shape[0] - 1):
                last_sig_qual = ann_num[sig_qual_pos[pos]]

            sig_qual_array[loc:] = ann_num[sig_qual_pos[pos]]

        # generate string for easy regex parsing
        beat_string = ''.join(beat_type.astype(str).tolist())

###############################################################################

# PATTERN MATCH CBD ###########################################################

        v_beat_array_cbd = (beat_type == "V")

        n3vn3_match_cbd = pattern_match(beat_string,
                                        n3vn3,
                                        beat_loc,
                                        sig_qual_array,
                                        3,
                                        6)

        if n3vn3_match_cbd.size == 0:

            srec += nrec
            num_loops_per_dataset += 1

            instance_stats_array = pd.concat([instance_stats_array,
                                              instance_stats],
                                             ignore_index=True,
                                             axis=0,
                                             sort=False)

            if FLAG_VERBOSE:
                print("no matching patterns in annotation chunk")

            continue

        if FLAG_VERBOSE:
            print("found matching patterns in annotation chunk")
            print("writing locations to text file")

        v_beat_loc = np.sort(np.array(list(set(n3vn3_match_cbd))))

        if FLAG_OUTPUT_DATA:
            for array_loc, beat_num in enumerate(v_beat_loc):

                # write location to text file
                f = open(UNIT_TEST_FILE, "a")
                f.write("%d\n" % (srec * fs + beat_loc[beat_num]))
                f.close()

                # initialize dat/atr file to store snippets
                TEST_FILE_BASE = DB \
                    + "_" \
                    + str(num_loops_per_dataset) \

                # write 30 second strip to annotation file

                data_segment = np.array([s1,
                                         s2]).T.astype(int)

                if DB == "MT21034724":
                    adc_gain = 390.10  # LWA
                else:
                    adc_gain = 682.67  # patch

                wfdb.wrsamp(record_name=TEST_FILE_BASE,
                            fs=fs,
                            units=["mV", "mV"],
                            sig_name=["0", "1"],
                            d_signal=data_segment,
                            fmt=["212", "212"],
                            adc_gain=[adc_gain, adc_gain],
                            baseline=[0, 0],
                            write_dir=".\\unit_test_data\\for irina\\")

                # write annotations within 30 second strip to file
                wfdb.wrann(record_name=TEST_FILE_BASE,
                           extension="atr",
                           sample=ann_loc.astype(int),
                           symbol=ann_type,
                           chan=ann_chan,
                           num=ann_num,
                           fs=fs,
                           write_dir=".\\unit_test_data\\for irina\\")

###############################################################################

# AGGREGATE NEW INFORMATION ###################################################

        pattern_stats["v_count_cbd"] += np.sum(v_beat_array_cbd)
        pattern_stats["n3vn3_count_cbd"] += n3vn3_match_cbd.size

        instance_stats["v_count_cbd"] = np.sum(v_beat_array_cbd)
        instance_stats["n3vn3_count_cbd"] = n3vn3_match_cbd.size

        instance_stats_array = pd.concat([instance_stats_array,
                                          instance_stats],
                                         ignore_index=True,
                                         axis=0,
                                         sort=False)

###############################################################################

        num_loops_per_dataset += 1
        srec += nrec

# RECORD STATISTICS ###########################################################

    if FLAG_VERBOSE:
        print("concatenating results to pattern_stats_array")

    pattern_stats_array = pd.concat([pattern_stats_array, pattern_stats],
                                    ignore_index=True,
                                    axis=0,
                                    sort=False)

    if FLAG_VERBOSE:
        print("Finished database %s" % DB)

###############################################################################

pattern_stats_array = pattern_stats_array[["database",
                                           "v_count_cbd",
                                           "n3vn3_count_cbd"]]

instance_stats_array = instance_stats_array[["database",
                                             "group",
                                             "v_count_cbd",
                                             "n3vn3_count_cbd"]]

if FLAG_VERBOSE:
    print("output pattern_stats_array to CSV file")

if FLAG_OUTPUT_DATA:
    csv_file = ".\\unit_test_data\\db_individual_stats_%s.csv" \
        % ("_".join(pattern_stats_array["database"].to_list()))
    pattern_stats_array.to_csv(path_or_buf=csv_file,
                               index=False)

    csv_file = ".\\unit_test_data\\instance_stats_%s.csv" \
        % ("_".join(pattern_stats_array["database"].to_list()))
    instance_stats_array.to_csv(path_or_buf=csv_file,
                                index=False)

if FLAG_VERBOSE:
    print("finished code!")
