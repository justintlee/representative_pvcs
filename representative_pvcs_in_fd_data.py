# IMPORT LIBRARIES ############################################################

import parse_methods
import os
import regex as re
import numpy as np
import pandas as pd
import v_filter as vf
import matplotlib.pyplot as plt

###############################################################################

# FUNCTIONS ###################################################################


def interpolate_time_stamps(ts, fs):

    num_samples = ts.shape[0]
    num_seconds = int(num_samples / fs)
    num_rem = num_samples % fs

    t = ts.copy()

    base_time_stamp_array = np.arange(fs) / fs

    for k in range(num_seconds):
        start_ind = fs * k
        end_ind = fs * (k + 1)

        t[start_ind:end_ind] = ts[start_ind:end_ind] + base_time_stamp_array

    if num_rem:
        t[end_ind:] = ts[end_ind:] + base_time_stamp_array[:num_rem]

    return t


def convert_ann_type(ann_type):

    ann_dict = {1: "N",
                5: "V",
                9: "A",
                13: "Q",
                12: "/",
                16: "|",
                39: "(",
                24: "p",
                40: ")",
                27: "t",
                29: "u",
                23: "=",
                14: "~",
                32: "[",
                33: "]",
                28: "+",
                22: '"',
                42: "C"}

    ann_type = np.array([ann_dict[k] for k in ann_type])

    return ann_type


def pattern_match(beat_string, pattern, beat_loc, offset):

    # match pattern
    match_loc = np.array([match.span()[0] - 1
                          for match in re.finditer(pattern,
                                                   beat_string,
                                                   overlapped=False)]) - offset

    # ensure prematurity and compensatory pause
    ind_to_delete = np.array([])
    for array_ind, beat_ind in enumerate(match_loc):

        # delete if there are no prior beats to evaluate for prematurity
        if beat_ind < 2:
            ind_to_delete = np.append(ind_to_delete, array_ind)

        # calculate premature ratio
        rr_current = beat_loc[beat_ind] - beat_loc[beat_ind - 1]
        rr_prev = beat_loc[beat_ind - 1] - beat_loc[beat_ind - 2]

        rr_ratio_premature = rr_current / rr_prev

        # if not premature then delete:
        if rr_ratio_premature > 0.8:
            ind_to_delete = np.append(ind_to_delete, array_ind)

        # ensure compensatory pause
        rr_next = beat_loc[beat_ind + 1] - beat_loc[beat_ind]

        rr_ratio_pause = rr_next / rr_prev

        # if no compensatory pause then delete:
        if rr_ratio_pause < 1.2:
            ind_to_delete = np.append(ind_to_delete, array_ind)

    # delete non-premature beats or beats that have no prior reference
    match_loc = np.delete(match_loc, ind_to_delete)

    return match_loc


###############################################################################

# SCRIPT PARAMETERS ###########################################################

# variables
BASE_DIR = "C:\\Users\\justin.lee\\Documents\\datasets\\2019-04 Field Data\\"
DB_ARRAY = [subfolder for subfolder in os.listdir(BASE_DIR)]
#DB_ARRAY = [DB_ARRAY[-1]]
fs = 250

# flags
FLAG_VERBOSE = True
FLAG_IMAGES = True

# image vector
FLAG_IMAGE_ON_STATE = []

###############################################################################

# STATISTICS STRUCTURE ########################################################

pattern_stats_array = pd.DataFrame(columns={"database",
                                            "v_count_cbd",
                                            "n2vn2_count_cbd",
                                            "n3vn3_count_cbd",
                                            "n4vn4_count_cbd",
                                            "n5vn5_count_cbd",
                                            "n6vn6_count_cbd",
                                            "nnvnv_count_cbd",
                                            "nnvnnv_count_cbd",
                                            "v_count_vf",
                                            "n2vn2_count_verified",
                                            "n3vn3_count_verified",
                                            "n4vn4_count_verified",
                                            "n5vn5_count_verified",
                                            "n6vn6_count_verified",
                                            "nnvnv_count_verified",
                                            "nnvnnv_count_verified"})

for DB in DB_ARRAY:

    print("DB: %s" % DB)

    FILE = BASE_DIR + DB + "\\ECG_DB.db"

    pattern_stats = pd.DataFrame(columns={"database",
                                          "v_count_cbd",
                                          "n2vn2_count_cbd",
                                          "n3vn3_count_cbd",
                                          "n4vn4_count_cbd",
                                          "n5vn5_count_cbd",
                                          "n6vn6_count_cbd",
                                          "nnvnv_count_cbd",
                                          "nnvnnv_count_cbd",
                                          "v_count_vf",
                                          "n2vn2_count_verified",
                                          "n3vn3_count_verified",
                                          "n4vn4_count_verified",
                                          "n5vn5_count_verified",
                                          "n6vn6_count_verified",
                                          "nnvnv_count_verified",
                                          "nnvnnv_count_verified"})

    pattern_stats["database"] = [DB]
    pattern_stats["v_count_cbd"] = [0]
    pattern_stats["n2vn2_count_cbd"] = [0]
    pattern_stats["n3vn3_count_cbd"] = [0]
    pattern_stats["n4vn4_count_cbd"] = [0]
    pattern_stats["n5vn5_count_cbd"] = [0]
    pattern_stats["n6vn6_count_cbd"] = [0]
    pattern_stats["nnvnv_count_cbd"] = [0]
    pattern_stats["nnvnnv_count_cbd"] = [0]
    pattern_stats["v_count_vf"] = [0]
    pattern_stats["n2vn2_count_verified"] = [0]
    pattern_stats["n3vn3_count_verified"] = [0]
    pattern_stats["n4vn4_count_verified"] = [0]
    pattern_stats["n5vn5_count_verified"] = [0]
    pattern_stats["n6vn6_count_verified"] = [0]
    pattern_stats["nnvnv_count_verified"] = [0]
    pattern_stats["nnvnnv_count_verified"] = [0]

    if FLAG_VERBOSE:
        print("initialized Pandas data structure")

###############################################################################

# nrec = 100
# srec = 1000

# LOAD DATA ###################################################################

    # list the regex strings for different patterns
    v = r"?<=(V)"
    n2vn2 = r"(?<=(NNVNN))"
    n3vn3 = r"(?<=(NNNVNNN))"
    n4vn4 = r"(?<=(NNNNVNNNN))"
    n5vn5 = r"(?<=(NNNNNVNNNNN))"
    n6vn6 = r"(?<=(NNNNNNVNNNNNN))"
    # n2vn2 = r"(?<=([^N]NNVNN|NNVNN[^N]))"
    # n3vn3 = r"(?<=([^N]NNNVNNN|NNNVNNN[^N]))"
    # n4vn4 = r"(?<=([^N]NNNNVNNNN|NNNNVNNNN[^N]))"
    # n5vn5 = r"(?<=([^N]NNNNNVNNNNN|NNNNNVNNNNN[^N]))"
    # n6vn6 = r"(?<=([^N]NNNNNNVNNNNNN|NNNNNNVNNNNNN[^N]))"
    nnvnv = r"(?<=(NNVNV))"
    nnvnnv = r"(?<=(NNVNNV))"

    nrec = 1000
    srec = 1000

    num_total_seconds = parse_methods.getNumPackets(FILE)

    if FLAG_VERBOSE:
        print("started loop")

    num_days = 7
    days_in_seconds = 60 * 60 * 24 * 7 + srec
    while (min(days_in_seconds, num_total_seconds) - srec) > nrec:

        if FLAG_VERBOSE:
            print("%s - %d" % (DB, srec))

        try:

            s1, s2, ts = parse_methods.getECGData(FILE, nrec, srec)

            t = interpolate_time_stamps(ts, fs)

            ann_loc, ann_type, ann_chan, ann_num = parse_methods.getANNData(FILE,
                                                                            nrec,
                                                                            srec)
            ann_loc_gt_num_samples = ann_loc >= s1.shape[0]
            if np.sum(ann_loc_gt_num_samples) > 0:
                first_loc_greater = np.where(ann_loc_gt_num_samples)[0][0]
                ann_loc = ann_loc[:first_loc_greater]
                ann_type = ann_type[:first_loc_greater]
                ann_chan = ann_chan[:first_loc_greater]
                ann_num = ann_num[:first_loc_greater]

        except Exception as exc:

            print(exc)
            srec += nrec
            continue

        ann_type = convert_ann_type(ann_type)

        # extract standard beats from the annotations
        standard_beat_types = ("V", "A", "N", "Q", "/", "|")
        beat_loc = ann_loc[[ann_inst in standard_beat_types
                            for ann_inst in ann_type]]
        beat_type = ann_type[[ann_inst in standard_beat_types
                              for ann_inst in ann_type]]
        beat_chan = ann_chan[[ann_inst in standard_beat_types
                              for ann_inst in ann_type]]
        beat_num = ann_num[[ann_inst in standard_beat_types
                            for ann_inst in ann_type]]

        # generate string for easy regex parsing
        beat_string = ''.join(beat_type.astype(str).tolist())

###############################################################################

# PATTERN MATCH CBD ###########################################################

        n2vn2_match_cbd = pattern_match(beat_string, n2vn2, beat_loc, 2)
        n3vn3_match_cbd = pattern_match(beat_string, n3vn3, beat_loc, 3)
        n4vn4_match_cbd = pattern_match(beat_string, n4vn4, beat_loc, 4)
        n5vn5_match_cbd = pattern_match(beat_string, n5vn5, beat_loc, 5)
        n6vn6_match_cbd = pattern_match(beat_string, n6vn6, beat_loc, 6)
        nnvnv_match_cbd = pattern_match(beat_string, nnvnv, beat_loc, 2)
        nnvnnv_match_cbd = pattern_match(beat_string, nnvnnv, beat_loc, 3)

        if n2vn2_match_cbd.size == 0 \
                and n3vn3_match_cbd.size == 0 \
                and n4vn4_match_cbd.size == 0 \
                and n5vn5_match_cbd.size == 0 \
                and n6vn6_match_cbd.size == 0 \
                and nnvnv_match_cbd.size == 0 \
                and nnvnnv_match_cbd.size == 0:

            srec += nrec

            if FLAG_VERBOSE:
                print("did not find matching patterns in original annotation data")

            continue

        if FLAG_VERBOSE:
            print("found matching patterns in original annotation data")

###############################################################################

# V BEAT FILTER ###############################################################

        if FLAG_VERBOSE:
            print("starting v beat detection")

        v_beat_array_cbd = (beat_type == "V")

        try:

            # feature extraction
            feat_mat, sf = vf.extract_features(s=s1,
                                               fs=fs,
                                               ann=beat_type,
                                               loc=beat_loc)

            # feature selection
            feat_for_class = feat_mat.iloc[:, 3:].copy()
            feat_for_class = feat_for_class.reset_index(drop=True)

            feat_for_class = vf.feat_select(feat_for_class=feat_for_class,
                                            v_beat_array=v_beat_array_cbd)

            # classification
            v_beat_array_vf = vf.classify_v(featMat=feat_for_class,
                                            ann=beat_type,
                                            loc=beat_loc,
                                            fs=fs,
                                            s=sf)

            v_beat_array_vf = vf.check_for_v_runs(class_array=v_beat_array_vf,
                                                  loc=beat_loc,
                                                  s=sf,
                                                  fs=fs,
                                                  orig_ann=v_beat_array_cbd,
                                                  f2ndFilter=True)

        except Exception as exc:

            print(exc)
            srec += nrec
            continue

        if FLAG_VERBOSE:
            print("finished v beat detection")

        beat_type_vf = beat_type.copy()
        beat_type_vf[beat_type_vf == "V"] = "N"
        beat_type_vf[v_beat_array_vf] = "V"

        v_beat_array_vf = (beat_type_vf == "V")

        beat_string_vf = ''.join(beat_type_vf.astype(str).tolist())

###############################################################################

# PATTERN MATCH VF ############################################################

        if FLAG_VERBOSE:
            print("finding patterns in new v beat results")

        n2vn2_match_vf = pattern_match(beat_string_vf, n2vn2, beat_loc, 2)
        n3vn3_match_vf = pattern_match(beat_string_vf, n3vn3, beat_loc, 3)
        n4vn4_match_vf = pattern_match(beat_string_vf, n4vn4, beat_loc, 4)
        n5vn5_match_vf = pattern_match(beat_string_vf, n5vn5, beat_loc, 5)
        n6vn6_match_vf = pattern_match(beat_string_vf, n6vn6, beat_loc, 6)
        nnvnv_match_vf = pattern_match(beat_string_vf, nnvnv, beat_loc, 2)
        nnvnnv_match_vf = pattern_match(beat_string_vf, nnvnnv, beat_loc, 3)

        intersect_n2vn2 = set(n2vn2_match_cbd).intersection(set(n2vn2_match_vf))
        intersect_n3vn3 = set(n3vn3_match_cbd).intersection(set(n3vn3_match_vf))
        intersect_n4vn4 = set(n4vn4_match_cbd).intersection(set(n4vn4_match_vf))
        intersect_n5vn5 = set(n5vn5_match_cbd).intersection(set(n5vn5_match_vf))
        intersect_n6vn6 = set(n6vn6_match_cbd).intersection(set(n6vn6_match_vf))
        intersect_nnvnv = set(nnvnv_match_cbd).intersection(set(nnvnv_match_vf))
        intersect_nnvnnv = set(nnvnnv_match_cbd).intersection(set(nnvnnv_match_vf))

        v_beat_loc = intersect_n2vn2.union(intersect_n3vn3,
                                           intersect_n4vn4,
                                           intersect_n5vn5,
                                           intersect_n6vn6,
                                           intersect_nnvnv,
                                           intersect_nnvnnv)

        v_beat_loc = np.sort(np.array(list(v_beat_loc)))

        if v_beat_loc.size > 0:

            if FLAG_VERBOSE:
                print("found patterns in new v beat results - printing images")

            for k in v_beat_loc:

                pattern_string = ""

                if k in intersect_n2vn2:
                    pattern_string = pattern_string + "-N2VN2-"
                if k in intersect_n3vn3:
                    pattern_string = pattern_string + "-N3VN3-"
                if k in intersect_n4vn4:
                    pattern_string = pattern_string + "-N4VN4-"
                if k in intersect_n5vn5:
                    pattern_string = pattern_string + "-N5VN5-"
                if k in intersect_n6vn6:
                    pattern_string = pattern_string + "-N6VN6-"
                if k in intersect_nnvnv:
                    pattern_string = pattern_string + "-NNVNV-"
                if k in intersect_nnvnnv:
                    pattern_string = pattern_string + "-NNVNNV-"

                try:

                    back_ind = max(beat_loc[k] - 4*fs, 0)
                    forw_ind = min(beat_loc[k] + 4*fs, s1.shape[0] - 1)

                    fig, ax = plt.subplots(nrows=2,
                                           ncols=1,
                                           sharex=True,
                                           sharey=False)

                    ax[0].plot(t[back_ind:forw_ind],
                               s1[back_ind:forw_ind])

                    for symbol in np.unique(beat_type):
                        marker_symbol = "$%s$" % (str(symbol))

                        # determine loctions of current symbol
                        beat_pos = [beat_sym == symbol
                                    for beat_sym in beat_type]
                        symb_loc = beat_loc[beat_pos]

                        ax[0].plot(t[symb_loc],
                                   s1[symb_loc] + 1,
                                   color="k",
                                   linestyle="None",
                                   marker=marker_symbol,
                                   markersize=5,
                                   markerfacecolor="k")

                    ax[0].plot(t[beat_loc[v_beat_array_vf]],
                               s1[beat_loc[v_beat_array_vf]] + 1,
                               color="r",
                               linestyle="None",
                               marker="$V$",
                               markersize=5,
                               markerfacecolor="r")

                    ax[0].set_xlim(t[back_ind], t[forw_ind])
                    ax[0].set_ylabel("amplitude")
                    ax[0].set_title("channel #0")

                    ax[1].plot(t[back_ind:forw_ind],
                               s2[back_ind:forw_ind])

                    for symbol in np.unique(beat_type):
                        marker_symbol = "$%s$" % (str(symbol))

                        # determine loctions of current symbol
                        beat_pos = [beat_sym == symbol
                                    for beat_sym in beat_type]
                        symb_loc = beat_loc[beat_pos]

                        ax[1].plot(t[symb_loc],
                                   s2[symb_loc] + 1,
                                   color="k",
                                   linestyle="None",
                                   marker=marker_symbol,
                                   markersize=5,
                                   markerfacecolor="k")

                    ax[1].plot(t[beat_loc[v_beat_array_vf]],
                               s2[beat_loc[v_beat_array_vf]] + 1,
                               color="r",
                               linestyle="None",
                               marker="$V$",
                               markersize=5,
                               markerfacecolor="r")

                    ax[1].set_xlim(t[back_ind], t[forw_ind])
                    ax[1].set_xlabel("timestamp (UTC)")
                    ax[1].set_ylabel("amplitude")
                    ax[1].set_title("channel #1")

                    fig_name = ".\\images\\%s%s%s.png" % (DB,
                                                          pattern_string,
                                                          t[beat_loc[k]])
                    plt.savefig(fig_name,
                                dpi=150,
                                bbox_inches="tight")
                    plt.close(fig)

                except Exception as exc:

                    print(exc)
                    srec += nrec
                    continue

        else:
            if FLAG_VERBOSE:
                print("did not find patterns in new v beat results")

###############################################################################

# AGGREGATE NEW INFORMATION ###################################################

        pattern_stats["v_count_cbd"] += np.sum(v_beat_array_cbd)
        pattern_stats["n2vn2_count_cbd"] += n2vn2_match_cbd.size
        pattern_stats["n3vn3_count_cbd"] += n3vn3_match_cbd.size
        pattern_stats["n4vn4_count_cbd"] += n4vn4_match_cbd.size
        pattern_stats["n5vn5_count_cbd"] += n5vn5_match_cbd.size
        pattern_stats["n6vn6_count_cbd"] += n6vn6_match_cbd.size
        pattern_stats["nnvnv_count_cbd"] += nnvnv_match_cbd.size
        pattern_stats["nnvnnv_count_cbd"] += nnvnnv_match_cbd.size
        pattern_stats["v_count_vf"] += np.sum(v_beat_array_vf)
        pattern_stats["n2vn2_count_verified"] += len(intersect_n2vn2)
        pattern_stats["n3vn3_count_verified"] += len(intersect_n3vn3)
        pattern_stats["n4vn4_count_verified"] += len(intersect_n4vn4)
        pattern_stats["n5vn5_count_verified"] += len(intersect_n5vn5)
        pattern_stats["n6vn6_count_verified"] += len(intersect_n6vn6)
        pattern_stats["nnvnv_count_verified"] += len(intersect_nnvnv)
        pattern_stats["nnvnnv_count_verified"] += len(intersect_nnvnnv)

###############################################################################

        srec += nrec

# RECORD STATISTICS ###########################################################

    if FLAG_VERBOSE:
        print("writing to text file")

    try:

        f = open("db_aggregate_stats.txt", "a")
        f.write("DB: %s\n" % (DB))
        f.write("N2VN2: %.2f\n" % (pattern_stats["n2vn2_count_verified"] / pattern_stats["n2vn2_count_cbd"] * 100))
        f.write("N3VN3: %.2f\n" % (pattern_stats["n3vn3_count_verified"] / pattern_stats["n3vn3_count_cbd"] * 100))
        f.write("N4VN4: %.2f\n" % (pattern_stats["n4vn4_count_verified"] / pattern_stats["n4vn4_count_cbd"] * 100))
        f.write("N5VN5: %.2f\n" % (pattern_stats["n5vn5_count_verified"] / pattern_stats["n5vn5_count_cbd"] * 100))
        f.write("N6VN6: %.2f\n" % (pattern_stats["n6vn6_count_verified"] / pattern_stats["n6vn6_count_cbd"] * 100))
        f.write("NNVNV: %.2f\n" % (pattern_stats["nnvnv_count_verified"] / pattern_stats["nnvnv_count_verified"] * 100))
        f.write("NNVNNV: %.2f\n" % (pattern_stats["nnvnnv_count_verified"] / pattern_stats["nnvnnv_count_verified"] * 100))
        f.close()

    except Exception as exc:

        print(exc)
        srec += nrec
        f.close()
        continue

    if FLAG_VERBOSE:
        print("concatenating results to pattern_stats_array")

    pattern_stats_array = pd.concat([pattern_stats_array, pattern_stats],
                                    ignore_index=True,
                                    axis=0,
                                    sort=False)

    if FLAG_VERBOSE:
        print("Finished database %s" % DB)

###############################################################################

pattern_stats_array = pattern_stats_array[["database",
                                           "v_count_cbd",
                                           "n2vn2_count_cbd",
                                           "n3vn3_count_cbd",
                                           "n4vn4_count_cbd",
                                           "n5vn5_count_cbd",
                                           "n6vn6_count_cbd",
                                           "nnvnv_count_cbd",
                                           "nnvnnv_count_cbd",
                                           "v_count_vf",
                                           "n2vn2_count_verified",
                                           "n3vn3_count_verified",
                                           "n4vn4_count_verified",
                                           "n5vn5_count_verified",
                                           "n6vn6_count_verified",
                                           "nnvnv_count_verified",
                                           "nnvnnv_count_verified"]]

if FLAG_VERBOSE:
    print("output pattern_stats_array to CSV file")

csv_file = "db_individual_stats_%s.csv" % ("_".join(pattern_stats_array["database"].to_list()))
pattern_stats_array.to_csv(path_or_buf=csv_file,
                           index=False)

if FLAG_VERBOSE:
    print("finished code!")
