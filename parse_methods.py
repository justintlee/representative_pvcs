#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import numpy as np


def int12(x):
    if x > 0xFFF:
        print(x)
        raise OverflowError
    if x > 0x7FF:
        x = -1*int(0x1000-x)
    return x


def int12r(x):

    x = int(x)

    if x >= 2**12/2:
        print(x)
        raise OverflowError

    if x < -1 * 2**12/2:
        print(x)
        raise OverflowError

    if x < 0:
        x = 0x800 | (0x7FF & x)
    else:
        x = 0x7FF & x

    return x


def write212File(fname, S1, S2):

    with open(fname, 'wb') as rb:
        for i in range(len(S1)):
            v1 = int12r(S1[i])
            v2 = int12r(S2[i])

            b = []
            b.append(0xFF & v1)
            b.append((v1 & 0xF00) >> 8 | (v2 & 0xF00) >> 4)
            b.append(0xFF & v2)

            rb.write(bytearray(b))


def parse212File(fname):

    with open(fname, "rb") as bfile:
        bytes = bytearray(bfile.read())

    blen = len(bytes)

    s = range(0, blen, 3)  # 24 bit values
    S1 = []
    S2 = []

    for ind in s:
        b = bytes[ind:ind+3]

        S1.append(int12(b[0] | ((b[1] & 0x0F) << 8)))
        S2.append(int12(((b[1] & 0xF0) << 4) | b[2]))

    S1 = np.array(S1)
    S2 = np.array(S2)

    return S1, S2


def parse212(bytes):

    blen = len(bytes)

    s = range(0, blen, 3)  # 24 bit values
    S1 = []
    S2 = []

    for ind in s:
        b = bytes[ind:ind+3]
        S1.append(int12(b[0] | ((b[1] & 0x0F) << 8)))
        S2.append(int12(((b[1] & 0xF0) << 4) | b[2]))

    S1 = np.array(S1)
    S2 = np.array(S2)

    return S1, S2


def connectdb(fname):
    try:
        conn = sqlite3.connect(fname)
    except sqlite3.Error as e:
        print(e)
        return
    finally:
        return conn
    return


def getECGData(dbfile, nrec, srec):

    conn = connectdb(dbfile)
    c = conn.cursor()
    sql = 'select EcgData, TimeStamp from EcgData'
    c.execute(sql)
    rows = c.fetchall()

    S1 = []
    S2 = []

    n = 0
    nrows = len(rows)
    #print('{} records'.format(len(rows)))

    if nrec is None:
        nrec = nrows - srec

    S1 = np.zeros(nrec*250)
    S2 = np.zeros(nrec*250)
    t = np.zeros(nrec*250)

    for row in rows:

        if n >= srec:
            i1 = (n-srec) * 250
            i2 = i1 + 250
            d = bytearray(row[0])
            s1t, s2t = parse212(d)
            S1[i1:i2] = s1t
            S2[i1:i2] = s2t
            t[i1:i2] = row[1]
        n += 1

        if n >= nrec+srec:
            break
    conn.close()
    return S1, S2, t


def getNumPackets(dbfile):
    conn = connectdb(dbfile)
    c = conn.cursor()
    sql = 'select TimeStamp from EcgData'
    c.execute(sql)
    rows = c.fetchall()
    numPackets = len(rows)
    conn.close()
    return numPackets


def getANNData(dbfile, nrec, srec):
    conn = connectdb(dbfile)
    c = conn.cursor()
    sql = 'select AnnotationData from AnnotationData'
    c.execute(sql)
    rows = c.fetchall()
    time = 'select TimeStamp from AnnotationData'
    c.execute(time)
    t = c.fetchall()
    conn.close()
    freq = 250

    sampleNum, typeNum, chanNum, num = [], [], [], []
    n = 0
    stStamp = t[srec][0]
    for row in rows:
        if n >= srec:
            d = bytearray(row[0])
            a1, a2, a3, a4 = parseANNPacket(d)
            a1 = np.array(a1)
            tdiff = t[n][0] - stStamp
            a1 = a1 + (tdiff * freq)
            sampleNum = sampleNum + list(a1)
            typeNum = typeNum + list(a2)
            chanNum = chanNum + list(a3)
            num = num + list(a4)
        n += 1
        if n >= nrec + srec:
            break
    sampleNum = np.array(sampleNum)
    typeNum = np.array(typeNum)
    chanNum = np.array(chanNum)
    num = np.array(num)

    return sampleNum, typeNum, chanNum, num


def parseANNPacket(data):

    a1, a2, a3, a4 = [], [], [], []
    n = 0
    for b in data:
        if b > 127:
            if (n % 4 == 0):
                out = int(b)
            else:
                out = int(b) - 256
        else:
            out = int(b)

        a = n % 4
        n += 1

        if a == 0:
            a1.append(out)
        if a == 1:
            a2.append(out)
        if a == 2:
            a3.append(out)
        if a == 3:
            a4.append(out)

    return a1, a2, a3, a4
