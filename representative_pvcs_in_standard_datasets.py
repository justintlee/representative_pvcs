# IMPORT LIBRARIES ############################################################

import wfdb
import os
import regex as re
import numpy as np
import pandas as pd

###############################################################################

# SCRIPT PARAMETERS ###########################################################

# variables
BASE_DIR = "C:\\Users\\justin.lee\\Documents\\datasets\\"
mit_dataset_array = ["MIT BIH\\" + subfolder
                     for subfolder in os.listdir(BASE_DIR + "MIT BIH\\")]
aha_dataset_array = ["AHA\\" + subfolder
                     for subfolder in os.listdir(BASE_DIR + "AHA\\")]

dataset_array = mit_dataset_array + aha_dataset_array
num_datasets = len(dataset_array)

# flags
FLAG_IMAGES = True

# image vector
FLAG_IMAGE_ON_STATE = []

###############################################################################

# GENERATE PATTERNS ###########################################################

# list the regex strings for different patterns
v = r"?<=(V)"
n1vn1 = r"(?<=((^|[^V])NVN($|[^V])))"
n2vn2 = r"(?<=(N{2}VN{2}))"
n3vn3 = r"(?<=(N{3}VN{3}))"
n4vn4 = r"(?<=(N{4}VN{4}))"
n5vn5 = r"(?<=(N{5}VN{5}))"
n6vn6 = r"(?<=(N{6}VN{6}))"
nvnv = r"(?<=(NVNV))"
nnvnnv = r"(?<=(NNVNNV))"

###############################################################################

pattern_stats = pd.DataFrame(columns={"database",
                                      "dataset",
                                      "v_present",
                                      "n1vn1_present",
                                      "n2vn2_present",
                                      "n3vn3_present",
                                      "n4vn4_present",
                                      "n5vn5_present",
                                      "n6vn6_present",
                                      "nvnv_present",
                                      "nnvnnv_present",
                                      "v_count",
                                      "n1vn1_count",
                                      "n2vn2_count",
                                      "n3vn3_count",
                                      "n4vn4_count",
                                      "n5vn5_count",
                                      "n6vn6_count",
                                      "nvnv_count",
                                      "nnvnnv_count"})

for dataset in dataset_array:

    pattern_inst = pd.DataFrame(columns={"database",
                                         "dataset",
                                         "v_present",
                                         "n1vn1_present",
                                         "n2vn2_present",
                                         "n3vn3_present",
                                         "n4vn4_present",
                                         "n5vn5_present",
                                         "n6vn6_present",
                                         "nvnv_present",
                                         "nnvnnv_present",
                                         "v_count",
                                         "n1vn1_count",
                                         "n2vn2_count",
                                         "n3vn3_count",
                                         "n4vn4_count",
                                         "n5vn5_count",
                                         "n6vn6_count",
                                         "nvnv_count",
                                         "nnvnnv_count"})

# LOAD ECG ANNOTATIONS ########################################################

    if "MIT" in dataset:
        dataset_sub = str(dataset[-3:None])
        pattern_inst["database"] = ["MIT"]
    elif "AHA" in dataset:
        dataset_sub = str(dataset[-5:None])
        pattern_inst["database"] = ["AHA"]
    elif "Petersburg" in dataset:
        dataset_sub = str(dataset[-7:None])
        pattern_inst["database"] = ["INCART"]
    else:
        dataset_sub = str(dataset[-9:None])
        pattern_inst["database"] = ["MSA"]

    pattern_inst["dataset"] = [dataset_sub]

    # Return annotations for the record:
    ann = wfdb.rdann(record_name=BASE_DIR + dataset + "\\" + dataset_sub,
                     extension="atr")

    # extract annotation parameters
    ann_loc = ann.sample
    ann_type = np.array(ann.symbol)

    # normalize annotations to be from {N, V, Q, F}
    # make R-on-T premature ventricular contraction
    # and ventricular escape beats ("E") as V"s
    ann_type[ann_type == "r"] = "V"
    ann_type[ann_type == "E"] = "V"

    # make left bundle branch block beat ("L"),
    # right bundle branch block beat ("R"),
    # bundle branch block beat ("B"),
    # atrial premature beat ("A"),
    # aberrated atrial premature beat ("a"),
    # nodal (junctional) premature bea ("J"),
    # supraventricular premature or ectopic beat (a
    # atrial escape beat ("e"),
    # and nodal (junctional) escape beat ("j") as N

    ann_type[ann_type == "L"] = "N"
    ann_type[ann_type == "R"] = "N"
    ann_type[ann_type == "B"] = "N"
    ann_type[ann_type == "A"] = "N"
    ann_type[ann_type == "a"] = "N"
    ann_type[ann_type == "J"] = "N"
    ann_type[ann_type == "S"] = "N"
    ann_type[ann_type == "e"] = "N"
    ann_type[ann_type == "j"] = "N"

    standard_beat_types = ("V", "F", "N", "Q")

    # extract standard beats from the annotations
    beat_loc = ann_loc[[ann_inst in standard_beat_types
                        for ann_inst in ann_type]]
    beat_type = ann_type[[ann_inst in standard_beat_types
                          for ann_inst in ann_type]]

    num_v = np.sum(beat_type == "V")
    pattern_inst["v_present"] = [num_v > 0]
    pattern_inst["v_count"] = [num_v]

    beat_string = ''.join(beat_type.astype(str).tolist())

###############################################################################

# PATTERN MATCH ###############################################################

    n1vn1_match = np.array([match.span()[0] - 1
                            for match in re.finditer(n1vn1,
                                                     beat_string,
                                                     overlapped=False)])
    n2vn2_match = np.array([match.span()[0] - 1
                            for match in re.finditer(n2vn2,
                                                     beat_string,
                                                     overlapped=False)])
    n3vn3_match = np.array([match.span()[0] - 1
                            for match in re.finditer(n3vn3,
                                                     beat_string,
                                                     overlapped=False)])
    n4vn4_match = np.array([match.span()[0] - 1
                            for match in re.finditer(n4vn4,
                                                     beat_string,
                                                     overlapped=False)])
    n5vn5_match = np.array([match.span()[0] - 1
                            for match in re.finditer(n5vn5,
                                                     beat_string,
                                                     overlapped=False)])
    n6vn6_match = np.array([match.span()[0] - 1
                            for match in re.finditer(n6vn6,
                                                     beat_string,
                                                     overlapped=False)])
    nvnv_match = np.array([match.span()[0] - 1
                           for match in re.finditer(nvnv,
                                                    beat_string,
                                                    overlapped=False)])
    nnvnnv_match = np.array([match.span()[0] - 1
                             for match in re.finditer(nnvnnv,
                                                      beat_string,
                                                      overlapped=False)])

    pattern_inst["n1vn1_present"] = n1vn1_match.size > 0
    pattern_inst["n2vn2_present"] = n2vn2_match.size > 0
    pattern_inst["n3vn3_present"] = n3vn3_match.size > 0
    pattern_inst["n4vn4_present"] = n4vn4_match.size > 0
    pattern_inst["n5vn5_present"] = n5vn5_match.size > 0
    pattern_inst["n6vn6_present"] = n6vn6_match.size > 0
    pattern_inst["nvnv_present"] = nvnv_match.size > 0
    pattern_inst["nnvnnv_present"] = nnvnnv_match.size > 0
    pattern_inst["n1vn1_count"] = n1vn1_match.size
    pattern_inst["n2vn2_count"] = n2vn2_match.size
    pattern_inst["n3vn3_count"] = n3vn3_match.size
    pattern_inst["n4vn4_count"] = n4vn4_match.size
    pattern_inst["n5vn5_count"] = n5vn5_match.size
    pattern_inst["n6vn6_count"] = n6vn6_match.size
    pattern_inst["nvnv_count"] = nvnv_match.size
    pattern_inst["nnvnnv_count"] = nnvnnv_match.size

    # concatenate dataset result matrix with database result matrix
    pattern_stats = pd.concat([pattern_stats, pattern_inst],
                              ignore_index=True,
                              axis=0,
                              sort=False)

###############################################################################

pattern_stats = pattern_stats[["database",
                               "dataset",
                               "v_present",
                               "n1vn1_present",
                               "n2vn2_present",
                               "n3vn3_present",
                               "n4vn4_present",
                               "n5vn5_present",
                               "n6vn6_present",
                               "nvnv_present",
                               "nnvnnv_present",
                               "v_count",
                               "n1vn1_count",
                               "n2vn2_count",
                               "n3vn3_count",
                               "n4vn4_count",
                               "n5vn5_count",
                               "n6vn6_count",
                               "nvnv_count",
                               "nnvnnv_count"]]
