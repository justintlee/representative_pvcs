# IMPORT LIBRARIES ############################################################

import parse_methods
import os
import regex as re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

###############################################################################

# FUNCTIONS ###################################################################


def interpolate_time_stamps(ts, fs):

    num_samples = ts.shape[0]
    num_seconds = int(num_samples / fs)
    num_rem = num_samples % fs

    t = ts.copy()

    base_time_stamp_array = np.arange(fs) / fs

    for k in range(num_seconds):
        start_ind = fs * k
        end_ind = fs * (k + 1)

        t[start_ind:end_ind] = ts[start_ind:end_ind] + base_time_stamp_array

    if num_rem:
        t[end_ind:] = ts[end_ind:] + base_time_stamp_array[:num_rem]

    return t


def ann_type_convert(ann_type):

    ann_dict = {1: "N",
                5: "V",
                9: "A",
                13: "Q",
                12: "/",
                16: "|",
                39: "(",
                24: "p",
                40: ")",
                27: "t",
                29: "u",
                23: "=",
                14: "~",
                32: "[",
                33: "]",
                28: "+",
                22: '"',
                42: "C"}

    ann_type = np.array([ann_dict[k] for k in ann_type])

    return ann_type


def pattern_match(beat_string, pattern, beat_loc,
                  sig_qual_info, offset, look_back):

    # match pattern
    match_loc = np.array([match.span()[0] - 1
                          for match in re.finditer(pattern,
                                                   beat_string,
                                                   overlapped=False)]) - offset

    # ensure prematurity and compensatory pause
    ind_to_delete = np.array([])
    for array_ind, beat_ind in enumerate(match_loc):

        # ensure that the first channel is clean for all beats
        pattern_qual = True

        for ind in range(look_back):
            beat_sig_qual = sig_qual_info[beat_loc[beat_ind - ind]] in (0, 2)
            pattern_qual = pattern_qual and beat_sig_qual

        if not pattern_qual:
            ind_to_delete = np.append(ind_to_delete, array_ind)

        # delete if there are no prior beats to evaluate for prematurity
        if beat_ind < 2:
            ind_to_delete = np.append(ind_to_delete, array_ind)

        # calculate premature ratio
        rr_current = beat_loc[beat_ind] - beat_loc[beat_ind - 1]
        rr_prev = beat_loc[beat_ind - 1] - beat_loc[beat_ind - 2]

        rr_ratio_premature = rr_current / rr_prev

        # if not premature then delete:
        if rr_ratio_premature > 0.8:
            ind_to_delete = np.append(ind_to_delete, array_ind)

        # ensure compensatory pause
        rr_next = beat_loc[beat_ind + 1] - beat_loc[beat_ind]

        rr_ratio_pause = rr_next / rr_prev

        # if no compensatory pause then delete:
        if rr_ratio_pause < 1.2:
            ind_to_delete = np.append(ind_to_delete, array_ind)

    # delete non-premature, non-compensatory beats, beats that are found in
    # unclean areas, or beats that have no prior reference
    match_loc = np.delete(match_loc, ind_to_delete)

    return match_loc


###############################################################################

# SCRIPT PARAMETERS ###########################################################

# variables
BASE_DIR = "C:\\Users\\justin.lee\\Documents\\datasets\\2019-04 Field Data\\"
DB_ARRAY = [subfolder for subfolder in os.listdir(BASE_DIR)]
fs = 250
NUM_DAYS = 7

# flags
FLAG_VERBOSE = True
FLAG_IMAGES = True

# image vector
FLAG_IMAGE_ON_STATE = []

###############################################################################

# STATISTICS STRUCTURE ########################################################

pattern_stats_array = pd.DataFrame(columns={"database",
                                            "v_count_cbd",
                                            "n2vn2_count_cbd",
                                            "n3vn3_count_cbd",
                                            "n4vn4_count_cbd",
                                            "n5vn5_count_cbd",
                                            "n6vn6_count_cbd",
                                            "nnvnv_count_cbd",
                                            "nnvnnv_count_cbd"})
for DB in DB_ARRAY:

    print("DB: %s" % DB)

    FILE = BASE_DIR + DB + "\\ECG_DB.db"

    pattern_stats = pd.DataFrame(columns={"database",
                                          "v_count_cbd",
                                          "n2vn2_count_cbd",
                                          "n3vn3_count_cbd",
                                          "n4vn4_count_cbd",
                                          "n5vn5_count_cbd",
                                          "n6vn6_count_cbd",
                                          "nnvnv_count_cbd",
                                          "nnvnnv_count_cbd"})

    pattern_stats["database"] = [DB]
    pattern_stats["v_count_cbd"] = [0]
    pattern_stats["n2vn2_count_cbd"] = [0]
    pattern_stats["n3vn3_count_cbd"] = [0]
    pattern_stats["n4vn4_count_cbd"] = [0]
    pattern_stats["n5vn5_count_cbd"] = [0]
    pattern_stats["n6vn6_count_cbd"] = [0]
    pattern_stats["nnvnv_count_cbd"] = [0]
    pattern_stats["nnvnnv_count_cbd"] = [0]

    if FLAG_VERBOSE:
        print("initialized Pandas data structure")

###############################################################################

# nrec = 100
# srec = 1000

# LOAD DATA ###################################################################

    # list the regex strings for different patterns
    v = r"?<=(V)"
    n2vn2 = r"(?<=(NNVNN))"
    n3vn3 = r"(?<=(NNNVNNN))"
    n4vn4 = r"(?<=(NNNNVNNNN))"
    n5vn5 = r"(?<=(NNNNNVNNNNN))"
    n6vn6 = r"(?<=(NNNNNNVNNNNNN))"
    nnvnv = r"(?<=(NNVNV))"
    nnvnnv = r"(?<=(NNVNNV))"

    # define constants used in every iteration
    nrec = 1000
    srec = 1000
    days_in_seconds = 60 * 60 * 24 * NUM_DAYS + srec
    last_sig_qual = 0

    num_total_seconds = parse_methods.getNumPackets(FILE)

    if FLAG_VERBOSE:
        print("started loop")

    while (min(days_in_seconds, num_total_seconds) - srec) > nrec:

        if FLAG_VERBOSE:
            print("%s - %d" % (DB, srec))

        try:

            s1, s2, ts = parse_methods.getECGData(FILE, nrec, srec)

            t = interpolate_time_stamps(ts, fs)

            num_samples = s1.shape[0]

            ann_loc, ann_type, ann_chan, ann_num = parse_methods.getANNData(FILE,
                                                                            nrec,
                                                                            srec)
            ann_loc_gt_num_samples = ann_loc >= s1.shape[0]
            if np.sum(ann_loc_gt_num_samples) > 0:
                first_loc_greater = np.where(ann_loc_gt_num_samples)[0][0]
                ann_loc = ann_loc[:first_loc_greater]
                ann_type = ann_type[:first_loc_greater]
                ann_chan = ann_chan[:first_loc_greater]
                ann_num = ann_num[:first_loc_greater]

        except Exception as exc:

            print(exc)
            srec += nrec
            continue

        ann_type = ann_type_convert(ann_type)

        # extract standard beats from the annotations
        standard_beat_types = ("V", "A", "N", "Q", "/", "|")
        beat_loc = ann_loc[[ann_inst in standard_beat_types
                            for ann_inst in ann_type]]
        beat_type = ann_type[[ann_inst in standard_beat_types
                              for ann_inst in ann_type]]
        beat_chan = ann_chan[[ann_inst in standard_beat_types
                              for ann_inst in ann_type]]
        beat_num = ann_num[[ann_inst in standard_beat_types
                            for ann_inst in ann_type]]

        # obtain a time series of signal quality values
        sig_qual_pos = np.where(np.logical_and(ann_type == "~",
                                               ann_chan == 108))[0]
        sig_qual_loc = ann_loc[sig_qual_pos]

        sig_qual_array = np.zeros(num_samples)

        for pos, loc in enumerate(sig_qual_loc):

            if pos == 0:
                sig_qual_array[:loc] = last_sig_qual

            elif pos == (sig_qual_loc.shape[0] - 1):
                last_sig_qual = ann_num[sig_qual_pos[pos]]

            sig_qual_array[loc:] = ann_num[sig_qual_pos[pos]]

        # generate string for easy regex parsing
        beat_string = ''.join(beat_type.astype(str).tolist())

###############################################################################

# PATTERN MATCH CBD ###########################################################

        v_beat_array_cbd = (beat_type == "V")

        n2vn2_match_cbd = pattern_match(beat_string,
                                        n2vn2,
                                        beat_loc,
                                        sig_qual_array,
                                        2,
                                        4)
        n3vn3_match_cbd = pattern_match(beat_string,
                                        n3vn3,
                                        beat_loc,
                                        sig_qual_array,
                                        3,
                                        6)
        n4vn4_match_cbd = pattern_match(beat_string,
                                        n4vn4,
                                        beat_loc,
                                        sig_qual_array,
                                        4,
                                        8)
        n5vn5_match_cbd = pattern_match(beat_string,
                                        n5vn5,
                                        beat_loc,
                                        sig_qual_array,
                                        5,
                                        10)
        n6vn6_match_cbd = pattern_match(beat_string,
                                        n6vn6,
                                        beat_loc,
                                        sig_qual_array,
                                        6,
                                        12)
        nnvnv_match_cbd = pattern_match(beat_string,
                                        nnvnv,
                                        beat_loc,
                                        sig_qual_array,
                                        2,
                                        4)
        nnvnnv_match_cbd = pattern_match(beat_string,
                                         nnvnnv,
                                         beat_loc,
                                         sig_qual_array,
                                         3,
                                         5)

        if n2vn2_match_cbd.size == 0 \
                and n3vn3_match_cbd.size == 0 \
                and n4vn4_match_cbd.size == 0 \
                and n5vn5_match_cbd.size == 0 \
                and n6vn6_match_cbd.size == 0 \
                and nnvnv_match_cbd.size == 0 \
                and nnvnnv_match_cbd.size == 0:

            srec += nrec

            if FLAG_VERBOSE:
                print("no matching patterns in annotation chunk")

            continue

        if FLAG_VERBOSE:
            print("found matching patterns in annotation chunk")
            print("printing images")

        v_beat_loc = set(n2vn2_match_cbd).union(set(n3vn3_match_cbd),
                                                set(n4vn4_match_cbd),
                                                set(n5vn5_match_cbd),
                                                set(n6vn6_match_cbd),
                                                set(nnvnv_match_cbd),
                                                set(nnvnnv_match_cbd))
        v_beat_loc = np.sort(np.array(list(v_beat_loc)))

        for k in v_beat_loc:

            pattern_string = ""

            if k in n2vn2_match_cbd:
                pattern_string = pattern_string + "-N2VN2-"
            if k in n3vn3_match_cbd:
                pattern_string = pattern_string + "-N3VN3-"
            if k in n4vn4_match_cbd:
                pattern_string = pattern_string + "-N4VN4-"
            if k in n5vn5_match_cbd:
                pattern_string = pattern_string + "-N5VN5-"
            if k in n6vn6_match_cbd:
                pattern_string = pattern_string + "-N6VN6-"
            if k in nnvnv_match_cbd:
                pattern_string = pattern_string + "-NNVNV-"
            if k in nnvnnv_match_cbd:
                pattern_string = pattern_string + "-NNVNNV-"

            try:

                back_ind = max(beat_loc[k] - 4*fs, 0)
                forw_ind = min(beat_loc[k] + 4*fs, s1.shape[0] - 1)

                fig, ax = plt.subplots(nrows=2,
                                       ncols=1,
                                       sharex=True,
                                       sharey=False)

                ax[0].plot(t[back_ind:forw_ind],
                           s1[back_ind:forw_ind])

                for symbol in np.unique(beat_type):
                    marker_symbol = "$%s$" % (str(symbol))

                    # determine loctions of current symbol
                    beat_pos = [beat_sym == symbol
                                for beat_sym in beat_type]
                    symb_loc = beat_loc[beat_pos]

                    ax[0].plot(t[symb_loc],
                               s1[symb_loc] + 1,
                               color="k",
                               linestyle="None",
                               marker=marker_symbol,
                               markersize=5,
                               markerfacecolor="k")

                ax[0].plot(t[beat_loc[beat_type == "V"]],
                           s1[beat_loc[beat_type == "V"]] + 1,
                           color="r",
                           linestyle="None",
                           marker="$V$",
                           markersize=5,
                           markerfacecolor="r")

                ax[0].set_xlim(t[back_ind], t[forw_ind])
                ax[0].set_ylabel("amplitude")
                ax[0].set_title("channel #0")

                ax[1].plot(t[back_ind:forw_ind],
                           s2[back_ind:forw_ind])

                for symbol in np.unique(beat_type):
                    marker_symbol = "$%s$" % (str(symbol))

                    # determine loctions of current symbol
                    beat_pos = [beat_sym == symbol
                                for beat_sym in beat_type]
                    symb_loc = beat_loc[beat_pos]

                    ax[1].plot(t[symb_loc],
                               s2[symb_loc] + 1,
                               color="k",
                               linestyle="None",
                               marker=marker_symbol,
                               markersize=5,
                               markerfacecolor="k")

                ax[1].plot(t[beat_loc[beat_type == "V"]],
                           s2[beat_loc[beat_type == "V"]] + 1,
                           color="r",
                           linestyle="None",
                           marker="$V$",
                           markersize=5,
                           markerfacecolor="r")

                ax[1].set_xlim(t[back_ind], t[forw_ind])
                ax[1].set_xlabel("timestamp (UTC)")
                ax[1].set_ylabel("amplitude")
                ax[1].set_title("channel #1")

                fig_name = ".\\images\\%s %s at %s.png" % (DB,
                                                           pattern_string,
                                                           t[beat_loc[k]] + srec)
                plt.savefig(fig_name,
                            dpi=150,
                            bbox_inches="tight")
                plt.close(fig)

            except Exception as exc:

                print(exc)
                srec += nrec
                continue

###############################################################################

# AGGREGATE NEW INFORMATION ###################################################

        pattern_stats["v_count_cbd"] += np.sum(v_beat_array_cbd)
        pattern_stats["n2vn2_count_cbd"] += n2vn2_match_cbd.size
        pattern_stats["n3vn3_count_cbd"] += n3vn3_match_cbd.size
        pattern_stats["n4vn4_count_cbd"] += n4vn4_match_cbd.size
        pattern_stats["n5vn5_count_cbd"] += n5vn5_match_cbd.size
        pattern_stats["n6vn6_count_cbd"] += n6vn6_match_cbd.size
        pattern_stats["nnvnv_count_cbd"] += nnvnv_match_cbd.size
        pattern_stats["nnvnnv_count_cbd"] += nnvnnv_match_cbd.size

###############################################################################

        srec += nrec

# RECORD STATISTICS ###########################################################

    if FLAG_VERBOSE:
        print("writing to text file")

    try:

        f = open("db_aggregate_stats.txt", "a")
        f.write("DB: %s\n" % (DB))
        f.write("N2VN2: %d\n" % pattern_stats["n2vn2_count_cbd"])
        f.write("N3VN3: %d\n" % pattern_stats["n3vn3_count_cbd"])
        f.write("N4VN4: %d\n" % pattern_stats["n4vn4_count_cbd"])
        f.write("N5VN5: %d\n" % pattern_stats["n5vn5_count_cbd"])
        f.write("N6VN6: %d\n" % pattern_stats["n6vn6_count_cbd"])
        f.write("NNVNV: %d\n" % pattern_stats["nnvnv_count_cbd"])
        f.write("NNVNNV: %d\n" % pattern_stats["nnvnnv_count_cbd"])
        f.close()

    except Exception as exc:

        print(exc)
        srec += nrec
        f.close()
        continue

    if FLAG_VERBOSE:
        print("concatenating results to pattern_stats_array")

    pattern_stats_array = pd.concat([pattern_stats_array, pattern_stats],
                                    ignore_index=True,
                                    axis=0,
                                    sort=False)

    if FLAG_VERBOSE:
        print("Finished database %s" % DB)

###############################################################################

pattern_stats_array = pattern_stats_array[["database",
                                           "v_count_cbd",
                                           "n2vn2_count_cbd",
                                           "n3vn3_count_cbd",
                                           "n4vn4_count_cbd",
                                           "n5vn5_count_cbd",
                                           "n6vn6_count_cbd",
                                           "nnvnv_count_cbd",
                                           "nnvnnv_count_cbd"]]

if FLAG_VERBOSE:
    print("output pattern_stats_array to CSV file")

csv_file = "db_individual_stats_%s.csv" % ("_".join(pattern_stats_array["database"].to_list()))
pattern_stats_array.to_csv(path_or_buf=csv_file,
                           index=False)

if FLAG_VERBOSE:
    print("finished code!")
